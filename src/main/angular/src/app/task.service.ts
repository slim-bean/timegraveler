import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {Project} from "./project.service";

@Injectable()
export class TaskService {

  constructor(public http:HttpClient) {

  }

  addTask(task:Task):Observable<any> {
    const path = '/v1/task';
    return this.http.post(path, JSON.stringify(task), {headers: new HttpHeaders().set('Content-Type', 'application/json')});
  }

  getTasks(filter?:string, projects?:Array<Project>, showOpen?:boolean, showClosed?:boolean, showFuture?:boolean):Observable<Task[]> {
    const path = '/v1/task';

    let queryParameters = new HttpParams();
    if (filter !== undefined) {
      queryParameters = queryParameters.set('filter', encodeURIComponent(filter));
    }
    if (projects !== undefined && projects.length > 0) {
      for (let project of projects) {
        queryParameters = queryParameters.append('project', String(project.projectId));
      }

    }

    if(showOpen !== undefined && showOpen) {
      queryParameters = queryParameters.append('state', 'OPEN');
    }
    if(showClosed !== undefined && showClosed) {
      queryParameters = queryParameters.append('state', 'CLOSED');
    }
    if(showFuture !== undefined && showFuture) {
      queryParameters = queryParameters.append('state', 'FUTURE');
    }

    return this.http.get<Task[]>(path, {headers: new HttpHeaders(), params: queryParameters});
  }


  reorderTask(taskId:number, newRank:number):Observable<any> {
    const path = '/v1/task/' + taskId + '/$reorder';

    var payload = {
      taskId: taskId,
      rank: newRank
    };

    return this.http.post(path, JSON.stringify(payload), {headers: new HttpHeaders().set('Content-Type', 'application/json')});
  }

  moveUp(taskId:number):Observable<any> {
    const path = '/v1/task/' + taskId + '/$moveUp';
    return this.http.post(path, null, {headers: new HttpHeaders().set('Content-Type', 'application/json')});
  }

  moveDown(taskId:number):Observable<any> {
    const path = '/v1/task/' + taskId + '/$moveDown';
    return this.http.post(path, null, {headers: new HttpHeaders().set('Content-Type', 'application/json')});
  }

  updateTask(task:Task):Observable<any> {
    const path = '/v1/task/' + task.taskId;
    return this.http.put(path, JSON.stringify(task), {headers: new HttpHeaders().set('Content-Type', 'application/json')});
  }

  closeTask(taskId:number):Observable<any> {
    const path = '/v1/task/' + taskId + '/$close';
    return this.http.post(path, null, {headers: new HttpHeaders().set('Content-Type', 'application/json')});
  }

}

export class Task {
  constructor(public taskName:string, public taskId:number, public project:number, public estimate:number, public rank:number, public state:string) {
  }
}

