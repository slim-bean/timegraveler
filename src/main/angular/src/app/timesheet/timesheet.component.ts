import {Component, OnInit, OnDestroy} from '@angular/core';
import {ProjectService} from "../project.service";
import {EventService, SummaryEvent} from "../event.service";
import {Subscription} from "rxjs";
import {HttpErrorResponse} from "@angular/common/http";
import {MessageService} from "primeng/components/common/messageservice";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-timesheet',
  templateUrl: './timesheet.component.html',
  styleUrls: ['./timesheet.component.css'],
  viewProviders: [EventService]
})
export class TimesheetComponent implements OnInit, OnDestroy {

  errorMessage:string;
  events:Array<SummaryEvent>;
  totalMins:number;
  windowMins:number;
  totalPercent:number;
  startDate: Date;
  endDate: Date;

  showHistoryDialog:boolean = false;
  historicalEvents:Array<SummaryEvent> = [];

  private projectSub:Subscription;

  constructor(private eventService:EventService,
              private projectService: ProjectService,
              private messageService: MessageService,
              private route: ActivatedRoute,
              private router: Router) {
    this.route.queryParams.subscribe(params => {
      if (params['startTime']){
        this.startDate = new Date(parseInt(params['startTime'],10));
        console.log("Events Page Start Time: " + this.startDate)
      }
      if (params['endTime']){
        this.endDate = new Date(parseInt(params['endTime'], 10));
        console.log("Events Page End Time: " + this.endDate)
      }
    });
  }

  ngOnInit() {
    if (!this.startDate) {
      this.startDate = new Date();
    }
    if (!this.endDate){
      this.endDate = new Date();
    }
    this.getSummaryEvents();
    this.projectSub = this.projectService.projectObservable.subscribe( (val) => {
      console.log("SummaryEvents project changed");
      this.getSummaryEvents();
    });
  }

  ngOnDestroy(){
    this.projectSub.unsubscribe();
  }

  getSummaryEvents() {
    this.startDate.setHours(0);
    this.startDate.setMinutes(0);
    this.endDate.setHours(23);
    this.endDate.setMinutes(59);
    this.endDate.setSeconds(59);
    this.eventService.getSummaryEvents(this.projectService.selectedProjects, null, this.startDate.getTime(), this.endDate.getTime())
      .subscribe(
        summaryEvents => {
          this.totalMins = 0;

          for(let event of summaryEvents){
            this.totalMins += event.totalMins;
          }

          this.windowMins = (this.endDate.getTime() - this.startDate.getTime())/(1000*60);

          let updatedEvents:Array<SummaryEvent> = [];
          for (let event of summaryEvents){
            event.percent = (event.totalMins/this.windowMins) * 100;
            updatedEvents.push(event);
          }

          this.totalPercent = 0;
          for (let event of updatedEvents){
            this.totalPercent += event.percent;
          }

          this.events = updatedEvents;
          this.messageService.add({severity:'success', summary:'SummaryEvents Loaded', detail:''})
        },
        (err: HttpErrorResponse) => {
          if (err.error instanceof Error) {
            console.log('An error occurred:', err.error.message);
            this.messageService.add({severity:'error', summary:err.error.message, detail:''})
          } else {
            console.log(`Backend returned code ${err.status}, body was: ${err.error.message}`);
            this.messageService.add({severity:'error', summary:err.error.message, detail:err.status.toString()})
          }
        }
      );
    this.router.navigate([],
      { relativeTo: this.route,
        queryParams: {"startTime": this.startDate ? this.startDate.getTime() : null,
          "endTime": this.endDate ? this.endDate.getTime() : null} });
  }

  loadAndDisplayHistoryOverlay(taskId:number){
    this.historicalEvents = [];
    this.showHistoryDialog = true;
    let start:Date = new Date(this.startDate);
    let end:Date = new Date(this.endDate);
    let diffDays = Math.round((this.endDate.getTime() - this.startDate.getTime()) / (1000 * 60 * 60 * 24));
    console.log("Looking up historical dates for a " + diffDays + " day interval");
    for (let i = 0; i < 5; i++) {
      this.eventService.getSummaryEvents(this.projectService.selectedProjects, [taskId], start.getTime(), end.getTime())
        .subscribe(
          summaryEvents => {
            if (summaryEvents && summaryEvents.length > 0) {
              this.historicalEvents.push(summaryEvents[0]);
              this.historicalEvents.sort( (event1, event2) => {
                if (event1.start > event2.start) {
                  return -1;
                } else if (event1.start < event2.start){
                  return 1;
                } else {
                  return 0;
                }
              })
            }
          },
          (err: HttpErrorResponse) => {
            if (err.error instanceof Error) {
              console.log('An error occurred:', err.error.message);
              this.messageService.add({severity:'error', summary:err.error.message, detail:''})
            } else {
              console.log(`Backend returned code ${err.status}, body was: ${err.error.message}`);
              this.messageService.add({severity:'error', summary:err.error.message, detail:err.status.toString()})
            }
          }
        );
      start.setDate(start.getDate() - diffDays);
      end.setDate(end.getDate() - diffDays);
    }


  }

}
