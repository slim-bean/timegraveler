import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {ModalDirective} from "ngx-bootstrap";
import {HttpErrorResponse} from "@angular/common/http";
import {Project, ProjectService} from "../project.service";

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {


  projectFilter:string;
  projects:Array<Project>;
  newProject:Project = new Project('', null, null);
  modalProject:Project;
  showOpen:boolean;
  showClosed:boolean;
  public status:{isopen:boolean} = {isopen: false};
  //FIXME this should not be hardcoded
  public states:Array<String> = ['OPEN', 'CLOSED'];

  @ViewChild('projectModal') projectModal:ModalDirective;

  constructor(private projectService:ProjectService) {
    this.projectFilter = '';
    this.modalProject = new Project('', null, '');
    this.showOpen = true;
    this.showClosed = false;
  }

  ngOnInit() {
    this.getProjects();
  }

  getProjects() {
    let stateArray: Array<string> = [];
    if (this.showOpen){
      stateArray.push('OPEN');
    }
    if (this.showClosed){
      stateArray.push('CLOSED');
    }
    this.projectService.getProjects(this.projectFilter, stateArray)
      .subscribe(
        projects => {
          this.projects = projects;
        },
        (err: HttpErrorResponse) => {
          if (err.error instanceof Error) {
            console.log('An error occurred:', err.error.message);
            // this.toastr.error(err.error.message);
          } else {
            console.log(`Backend returned code ${err.status}, body was: ${err.error.message}`);
            // this.toastr.error(err.error.message, err.status.toString());
          }
        }
      )
  }

  submitNewProject(){
    console.log("Adding new project: " + this.newProject);
    this.projectService.addProject(this.newProject)
      .subscribe(
        () => {
          console.log('New Project Created');
          this.getProjects();
          this.newProject.projectName = '';
          this.projectService.reload();
        },
        (err: HttpErrorResponse) => {
          if (err.error instanceof Error) {
            console.log('An error occurred:', err.error.message);
            // this.toastr.error(err.error.message);
          } else {
            console.log(`Backend returned code ${err.status}, body was: ${err.error.message}`);
            // this.toastr.error(err.error.message, err.status.toString());
          }
        }
      );
  }

  onFilterSubmit() {
    this.getProjects();
  }

  editProject(project:Project) {
    this.modalProject = project;
    this.projectModal.show();
  }

  closeProject(projectId:number) {
    let proj:Project = new Project(null, projectId, 'CLOSED');
    this.projectService.updateProject(proj)
      .subscribe(
        () => {
          console.log('Project Closed');
          this.projectModal.hide();
          this.getProjects();
          this.projectService.reload();
        },
        (err: HttpErrorResponse) => {
          if (err.error instanceof Error) {
            console.log('An error occurred:', err.error.message);
            // this.toastr.error(err.error.message);
          } else {
            console.log(`Backend returned code ${err.status}, body was: ${err.error.message}`);
            // this.toastr.error(err.error.message, err.status.toString());
          }
        }
      );
  }

  updateProject() {
    this.projectService.updateProject(this.modalProject)
      .subscribe(
        () => {
          console.log('Project Updated');
          this.projectModal.hide();
          this.getProjects();
          this.projectService.reload();
        },
        (err: HttpErrorResponse) => {
          if (err.error instanceof Error) {
            console.log('An error occurred:', err.error.message);
            // this.toastr.error(err.error.message);
          } else {
            console.log(`Backend returned code ${err.status}, body was: ${err.error.message}`);
            // this.toastr.error(err.error.message, err.status.toString());
          }
        }
      );
  }

  changeOpen(){
    this.showOpen = !this.showOpen;
    this.getProjects();
  }

  changeClosed(){
    this.showClosed = !this.showClosed;
    this.getProjects();
  }

}
