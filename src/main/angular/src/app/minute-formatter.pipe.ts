import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'minuteFormatter'
})
export class MinuteFormatterPipe implements PipeTransform {

  transform(value: number, args?: any): String {
    if (value < 60){
      return String(value + " m");
    }
    return String((Math.round( (value/60) * 10) / 10) + " h");
  }

}
