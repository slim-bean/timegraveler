import {Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {Subscription} from "rxjs";
import {ProjectService} from "../project.service";
import {EventService, Event} from "../event.service";
import {HttpErrorResponse} from "@angular/common/http";
import {TimePickerTime} from "../timepicker/timepicker.component";
import {ConfirmationService} from "primeng/primeng";
import {MessageService} from "primeng/components/common/messageservice";

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css'],
  providers: [ConfirmationService],
  viewProviders: [EventService]
})
export class EventsComponent implements OnInit, OnDestroy, OnChanges {

  errorMessage: string;
  events: Array<Event>;
  eventDate;
  showEditEventDialog: boolean = false;
  modalTitle: string;
  modalEventTime: number;
  modalNotes: string;
  displayTimePicker: boolean = false;
  timePickerTime: TimePickerTime;
  datePickerDate: Date;
  private projectSub: Subscription;

  @Input()
  showEventRemoveButton:boolean;

  @Input()
  showEventEditButton:boolean;

  @Input()
  showAllProjects:boolean;

  @Input()
  showNotes:boolean;

  @Input()
  filterByProject:boolean;

  private _startTime:Date;

  private _endTime:Date;

  @Input()
  taskIds: Array<number>;

  @Input()
  paginated: boolean;

  totalRecords: number;


  constructor(private eventService: EventService,
              private projectService: ProjectService,
              private messageService: MessageService,
              private confirmationService: ConfirmationService) {
    this.eventDate = new Date();
    this.modalTitle = '';
    this.showEventRemoveButton = true;
    this.showEventEditButton = true;
    this.showAllProjects = false;
    this.showNotes = false;
    this.filterByProject = false;
    this.paginated = false;
  }

  ngOnInit() {
    //this.getEvents();
    this.projectSub = this.projectService.projectObservable.subscribe(() => {
      console.log("Selected Project Changed, Get Events");
      this.getEvents();
    });
  }

  ngOnDestroy() {
    this.projectSub.unsubscribe();
  }

  ngOnChanges(changes: SimpleChanges): void {

    if (this.paginated) {
      this.getCount();
      this.getEvents();
    } else {
      this.getEvents();
    }
  }



  getProjectName(projectId:number): string {
    return this.projectService.getProjectName(projectId)
  }

  loadData(event){
    console.log("data load requested for starting row: " + event.first + " and count: " + event.rows);
    this.getEvents(event.rows, event.first);
  }

  getCount(){
    let projects = null;
    if (!this.showAllProjects) {
      projects = this.projectService.selectedProjects;
    }
    this.eventService.countEvents(projects, this.taskIds, this._startTime, this._endTime)
      .subscribe(
        count => {
          console.log("Found " + count + " total events");
          this.totalRecords = count;
        },
        (err: HttpErrorResponse) => {
          if (err.error instanceof Error) {
            console.log('An error occurred:', err.error.message);
            this.messageService.add({severity:'error', summary:err.error.message, detail:''})
          } else {
            console.log(`Backend returned code ${err.status}, body was: ${err.error.message}`);
            this.messageService.add({severity:'error', summary:err.error.message, detail:err.status.toString()})
          }
        }
      );
  }

  getEvents(limit?:number, offset?:number) {
    let projects = null;
    if (!this.showAllProjects) {
      projects = this.projectService.selectedProjects;
    }
    this.eventService.searchEvents(projects, this.taskIds, this._startTime, this._endTime, limit, offset)
      .subscribe(
        trackers => {
          this.events = trackers;
          this.messageService.add({severity:'success', summary:'Events Loaded', detail:''})
        },
        (err: HttpErrorResponse) => {
          if (err.error instanceof Error) {
            console.log('An error occurred:', err.error.message);
            this.messageService.add({severity:'error', summary:err.error.message, detail:''})
          } else {
            console.log(`Backend returned code ${err.status}, body was: ${err.error.message}`);
            this.messageService.add({severity:'error', summary:err.error.message, detail:err.status.toString()})
          }
        }
      );
  }

  showEvent(event:Event) {
    this.modalEventTime = event.start;
    this.modalTitle = event.taskName;
    this.modalNotes = event.notes;
    let date = new Date(event.start);
    this.eventDate.hour = date.getHours();
    this.eventDate.minute = date.getMinutes();
    this.eventDate.seconds = date.getSeconds();
    this.datePickerDate = new Date(event.start);
    this.showEditEventDialog = true;
  }

  showTimePicker(){
    this.timePickerTime = {
      hour: this.eventDate.hour,
      minute: this.eventDate.minute
    };
    this.displayTimePicker = true;
  }

  setTime(event:TimePickerTime){
    this.eventDate.hour = event.hour;
    this.eventDate.minute = event.minute;
    this.displayTimePicker = false;
  }

  hideModal() {
    this.displayTimePicker = false;
  }

  updateEvent() {
    //We need to add the time value to the datePickerDate before we submit the event.
    this.datePickerDate.setHours(this.eventDate.hour);
    this.datePickerDate.setMinutes(this.eventDate.minute);
    this.datePickerDate.setSeconds(this.eventDate.seconds);
    this.eventService.updateEvent(this.modalEventTime, new Event(null, null, this.datePickerDate.getTime(), null, null, this.modalNotes))
      .subscribe(
        result => {
          console.log("Succesfully updated " + this.modalTitle + " refreshing events list");
          this.getEvents();
          this.showEditEventDialog = false;
        },
        (err: HttpErrorResponse) => {
          if (err.error instanceof Error) {
            console.log('An error occurred:', err.error.message);
            this.messageService.add({severity:'error', summary:err.error.message, detail:''})
          } else {
            console.log(`Backend returned code ${err.status}, body was: ${err.error.message}`);
            this.messageService.add({severity:'error', summary:err.error.message, detail:err.status.toString()})
          }
        }
      )
  }

  confirmRemoveEvent(event:Event) {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete event "' + event.taskName + '" which started at "' + new Date(event.start) + '"',
      accept: () => {
        this.removeEvent(event.start)
      }
    });
  }

  removeEvent(eventId:number) {
    this.eventService.removeEvent(eventId)
      .subscribe(
        result => {
          this.getEvents();
        },
        (err: HttpErrorResponse) => {
          if (err.error instanceof Error) {
            console.log('An error occurred:', err.error.message);
            this.messageService.add({severity:'error', summary:err.error.message, detail:''})
          } else {
            console.log(`Backend returned code ${err.status}, body was: ${err.error.message}`);
            this.messageService.add({severity:'error', summary:err.error.message, detail:err.status.toString()})
          }
        }
      )
  }

  trackTask(taskId: number) {
    this.eventService.track(taskId)
      .subscribe(
        result => {
          this.getEvents()
        },
        (err: HttpErrorResponse) => {
          if (err.error instanceof Error) {
            console.log('An error occurred:', err.error.message);
            this.messageService.add({severity:'error', summary:err.error.message, detail:''})
          } else {
            console.log(`Backend returned code ${err.status}, body was: ${err.error.message}`);
            this.messageService.add({severity:'error', summary:err.error.message, detail:err.status.toString()})
          }
        }
      )
  }

  @Input()
  set startTime(value: Date) {
    this._startTime = value;
    //this.getEvents();
  }

  @Input()
  set endTime(value: Date) {
    this._endTime = value;
    //this.getEvents();
  }
}
