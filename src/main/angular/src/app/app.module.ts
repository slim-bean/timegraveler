import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {AppComponent} from './app.component';
import {AlertModule, ModalModule, BsDropdownModule, CollapseModule} from "ngx-bootstrap";
import {TasksComponent} from './tasks/tasks.component';
import {RouterModule, Routes} from "@angular/router";
import {ProjectService} from "./project.service";
import {EventsComponent} from './events/events.component';
import {TrackerComponent} from './tracker/tracker.component';
import {TimesheetComponent} from './timesheet/timesheet.component';
import {HttpClientModule} from "@angular/common/http";
import {MultiSelectModule} from 'primeng/multiselect';
import {DropdownModule} from 'primeng/dropdown';
import {DialogModule} from 'primeng/dialog';
import {CalendarModule} from 'primeng/calendar';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {GrowlModule} from 'primeng/growl';
import {DataViewModule} from 'primeng/dataview';
import { ProjectsComponent } from './projects/projects.component';
import { TimepickerComponent } from './timepicker/timepicker.component';
import {MessageService} from "primeng/components/common/messageservice";
import { EventsPageComponent } from './events-page/events-page.component';
import {OverlayPanelModule} from 'primeng/overlaypanel';
import { MinuteFormatterPipe } from './minute-formatter.pipe';
import {ButtonModule} from "primeng/button";
import {InputTextModule} from "primeng/primeng";
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { PwaComponent } from './pwa/pwa.component';
import {PwaService} from "./pwa.service";

const appRoutes: Routes = [
  {path: '',   redirectTo: '/tracker', pathMatch: 'full'},
  {path: 'tasks', component: TasksComponent},
  {path: 'events', component: EventsPageComponent},
  {path: 'tracker', component: TrackerComponent},
  {path: 'projects', component: ProjectsComponent},
  {path: 'timesheet', component: TimesheetComponent},
  {path: 'tp', component: TimepickerComponent},
  {path: 'pwa', component: PwaComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    TasksComponent,
    EventsComponent,
    TrackerComponent,
    TimesheetComponent,
    ProjectsComponent,
    TimepickerComponent,
    EventsPageComponent,
    MinuteFormatterPipe,
    PwaComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    AlertModule.forRoot(),
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    CollapseModule.forRoot(),
    MultiSelectModule,
    DropdownModule,
    DialogModule,
    CalendarModule,
    ConfirmDialogModule,
    GrowlModule,
    DataViewModule,
    OverlayPanelModule,
    ButtonModule,
    InputTextModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [ProjectService, MessageService, PwaService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
