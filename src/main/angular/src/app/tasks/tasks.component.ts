import {Component, OnInit, EventEmitter, Output, Input, ViewChild, OnDestroy} from '@angular/core';
import {Subscription} from "rxjs";
import {Task, TaskService} from "../task.service";
import {ModalDirective} from "ngx-bootstrap";
import {Project, ProjectService} from "../project.service";
import {HttpErrorResponse} from '@angular/common/http';
import {MessageService} from "primeng/components/common/messageservice";
import {SelectItem} from "primeng/api";

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css'],
  viewProviders: [TaskService]
})
export class TasksComponent implements OnInit, OnDestroy {

  taskFilter:string;
  tasks:Array<Task>;
  allProjects:Array<Project>;
  projects:Array<Project>;
  selectedProject:Project;
  errorMessage:string;
  newTask:Task = new Task('', null, null, null, null, null);
  modalTask:Task;
  modalProject:Project;
  modalState:string;
  showTaskModal:boolean = false;
  showOpen:boolean;
  showClosed:boolean;
  showFuture:boolean;
  private projectSub:Subscription;

  public states:SelectItem[];


  @Input()
  showStartTrackingButton:boolean;

  @Output()
  trackTask:EventEmitter<TaskEvent>;

  constructor(private taskService:TaskService,
              private projectService:ProjectService,
              private messageService: MessageService) {
    this.taskFilter = '';
    this.trackTask = new EventEmitter<TaskEvent>();
    this.showStartTrackingButton = false;
    this.modalTask = new Task('', null, null, null, null, null);
    this.showOpen = true;
    this.showClosed = false;
    this.showFuture = false;

    //FIXME this should not be hardcoded
    this.states = [
      {label: "OPEN", value: "OPEN"},
      {label: "CLOSED", value: "CLOSED"},
      {label: "FUTURE", value: "FUTURE"}
    ]
  }

  ngOnInit() {
    this.getTasks();
    this.allProjects = this.projectService.allOpenProjects;
    this.projects = this.projectService.selectedProjects;
    if (this.projects.length > 0) {
      this.selectedProject = this.projects[0];
    }
    this.projectSub = this.projectService.projectObservable.subscribe(() => {
      console.log("Selected projects changed, reloading tasks");
      this.projects = this.projectService.selectedProjects;
      this.allProjects = this.projectService.allOpenProjects;
      if (this.projects.length > 0) {
        this.selectedProject = this.projects[0];
      }
      this.getTasks()
    });
  }

  ngOnDestroy() {
    this.projectSub.unsubscribe();
  }

  getProjectName(projectId:number): string {
    return this.projectService.getProjectName(projectId)
  }

  startTrackingTask(taskId:number) {
    this.trackTask.emit(new TaskEvent(taskId));
  }

  getTasks() {
    this.taskService.getTasks(this.taskFilter, this.projectService.selectedProjects, this.showOpen, this.showClosed, this.showFuture)
      .subscribe(
        tasks => {
          this.tasks = tasks
          this.messageService.add({severity:'success', summary:'Tasks Loaded', detail:''})
        },
        (err: HttpErrorResponse) => {
          if (err.error instanceof Error) {
            console.log('An error occurred:', err.error.message);
            this.messageService.add({severity:'error', summary:err.error.message, detail:''})
          } else {
            console.log(`Backend returned code ${err.status}, body was: ${err.error.message}`);
            this.messageService.add({severity:'error', summary:err.error.message, detail:err.status.toString()})
          }
        }
      );
    // this.tasks = [
    //   new Task('pee', 1, 1, 1, 'OPEN'),
    //   new Task('pee', 2, 1, 2, 'OPEN'),
    //   new Task('pee', 3, 1, 3, 'OPEN')
    // ]
  }

  submitNewTask() {
    this.newTask.project = this.selectedProject.projectId;
    console.log(this.newTask);
    this.taskService.addTask(this.newTask)
      .subscribe(
        () => {
          this.getTasks();
          this.newTask.taskName = '';
          this.newTask.project = null;
        },
        (err: HttpErrorResponse) => {
          if (err.error instanceof Error) {
            console.log('An error occurred:', err.error.message);
            this.messageService.add({severity:'error', summary:err.error.message, detail:''})
          } else {
            console.log(`Backend returned code ${err.status}, body was: ${err.error.message}`);
            this.messageService.add({severity:'error', summary:err.error.message, detail:err.status.toString()})
          }
        }
      )
  }

  onFilterSubmit() {
    this.getTasks();
  }

  editTask(task:Task) {
    this.modalTask = task;
    this.modalProject = this.projectService.getProjectById(task.project);
    this.modalState = task.state;
    this.showTaskModal = true;
  }

  closeTask(taskId:number) {
    this.taskService.closeTask(taskId)
      .subscribe(
        result => this.getTasks(),
        error => this.errorMessage = <any>error
      )
  }

  updateTask() {
    console.log('Submitting task ' + this.modalTask.taskId + ' with new name ' + this.modalTask.taskName);
    this.taskService.updateTask(this.modalTask)
      .subscribe(
        result => {
          this.showTaskModal = false;
          this.getTasks();
        },
        (err: HttpErrorResponse) => {
          if (err.error instanceof Error) {
            console.log('An error occurred:', err.error.message);
            this.messageService.add({severity:'error', summary:err.error.message, detail:''})
          } else {
            console.log(`Backend returned code ${err.status}, body was: ${err.error.message}`);
            this.messageService.add({severity:'error', summary:err.error.message, detail:err.status.toString()})
          }
        }
      )
  }

  changeOpen(){
    this.showOpen = !this.showOpen;
    this.getTasks();
  }

  changeClosed(){
    this.showClosed = !this.showClosed;
    this.getTasks();
  }

  changeFuture(){
    this.showFuture = !this.showFuture;
    this.getTasks();
  }

  dragStart(event:DragEvent, taskId:number):void {
    console.log('Drag Start for task ' + taskId)
    event.dataTransfer.setData("taskId", <any>taskId);
  }

  dragOver(event:DragEvent):void {
    event.preventDefault()
  }

  dragDrop(event:DragEvent, newRank:number):void {
    console.log('Drag Stop at new rank ' + newRank);
    let taskId:number = +event.dataTransfer.getData("taskId");
    this.taskService.reorderTask(taskId, newRank)
      .subscribe(
        () => this.getTasks(),
        (err: HttpErrorResponse) => {
          if (err.error instanceof Error) {
            console.log('An error occurred:', err.error.message);
            this.messageService.add({severity:'error', summary:err.error.message, detail:''})
          } else {
            console.log(`Backend returned code ${err.status}, body was: ${err.error.message}`);
            this.messageService.add({severity:'error', summary:err.error.message, detail:err.status.toString()})
          }
        }
      )
  }

  moveUp(task:Task) {
    for (let i = 0; i < this.tasks.length; i++) {
      if (this.tasks[i].taskId == task.taskId) {
        if (i == 0) {
          console.log("Already highest rank");
          return;
        }
        let newRank = this.tasks[i-1].rank;
        console.log('Task ' + task.taskName + ' moving from ' + task.rank + ' to ' +  newRank);
        this.taskService.reorderTask(task.taskId, newRank)
          .subscribe(
            () => this.getTasks(),
            (err: HttpErrorResponse) => {
              if (err.error instanceof Error) {
                console.log('An error occurred:', err.error.message);
                this.messageService.add({severity:'error', summary:err.error.message, detail:''})
              } else {
                console.log(`Backend returned code ${err.status}, body was: ${err.error.message}`);
                this.messageService.add({severity:'error', summary:err.error.message, detail:err.status.toString()})
              }
            }
          )
      }
    }
  }

  moveDown(task:Task) {
    for (let i = 0; i < this.tasks.length; i++) {
      if (this.tasks[i].taskId == task.taskId) {
        if (i == this.tasks.length - 1) {
          console.log("Already lowest rank");
          return;
        }
        let newRank = this.tasks[i+1].rank;
        console.log('Task ' + task.taskName + ' moving from ' + task.rank + ' to ' +  newRank);
        this.taskService.reorderTask(task.taskId, newRank)
          .subscribe(
            () => this.getTasks(),
            (err: HttpErrorResponse) => {
              if (err.error instanceof Error) {
                console.log('An error occurred:', err.error.message);
                this.messageService.add({severity:'error', summary:err.error.message, detail:''})
              } else {
                console.log(`Backend returned code ${err.status}, body was: ${err.error.message}`);
                this.messageService.add({severity:'error', summary:err.error.message, detail:err.status.toString()})
              }
            }
          )
      }
    }
  }

}

export class TaskEvent {
  constructor(public taskId:number) {
  }

}
