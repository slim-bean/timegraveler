import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';

enum ClockTypeEnum {
  Hour,
  Min
}

export interface TimePickerTime {
  hour: number;
  minute: number;
}

@Component({
  selector: 'app-timepicker',
  templateUrl: './timepicker.component.html',
  styleUrls: ['./timepicker.component.scss']
})
export class TimepickerComponent implements OnInit, OnChanges {

  steps:number[];
  private amIsSet:Boolean;
  private clockType:ClockTypeEnum;

  @Input() public time: TimePickerTime;
  @Output() public timePicked: EventEmitter<TimePickerTime> = new EventEmitter();


  constructor() {

  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.time){
      console.log("TimePicker input changed. Hour: " + this.time.hour + ", Min: " + this.time.minute);
      this.clockType = ClockTypeEnum.Hour;
      this.steps = this.setSteps(this.clockType);
      this.amIsSet = this.time.hour <= 11;
    }
  }



  ngOnInit() {
    if (!this.time) {
      this.time = {
        hour: 0,
        minute: 0,
      }
    }
    this.amIsSet = this.time.hour <= 11;
    this.clockType = ClockTypeEnum.Hour;
    this.steps = this.setSteps(this.clockType);
  }


  private getClass(index: number) {
    return 'clock-step clock-loc-deg' + (30 * (index + 1));
  }

  private getButtonClass(step: number){
    if (this.clockType == ClockTypeEnum.Hour) {
      let hr: number;
      if (this.time.hour > 12) {
        hr = this.time.hour - 12;
      } else if (this.time.hour == 0) {
        hr = 12;
      } else {
        hr = this.time.hour;
      }
      if (step == hr) {
        return "btn-round btn-selected";
      } else {
        return "btn-round";
      }
    } else {
      let min: number = Math.round(this.time.minute/5)*5;
      if (step == min) {
        return "btn-round btn-selected";
      } else {
        return "btn-round";
      }
    }

  }

  isClockTypeHours(): boolean {
    if (this.clockType == ClockTypeEnum.Hour){
      return true;
    } else {
      return false;
    }
  }

  amSet() {
    this.amIsSet = true;
  }

  pmSet() {
    this.amIsSet = false;
  }

  pickTime(step:number){
    if (this.clockType == ClockTypeEnum.Hour){
      this.time.hour = step;
      if (!this.amIsSet && step != 12){
        this.time.hour = this.time.hour + 12;
      } else if (this.amIsSet && step == 12) {
        this.time.hour = 0;
      }
      console.log("TimePicker selected hour: " + this.time.hour);
      this.clockType = ClockTypeEnum.Min;
      this.steps = this.setSteps(this.clockType);
    } else {
      this.time.minute = step;
      console.log("TimePicker selected minute: " + this.time.minute);
      this.timePicked.emit(this.time);
      this.clockType = ClockTypeEnum.Hour;
      this.steps = this.setSteps(this.clockType);
    }
  }

  private setSteps(clockType:ClockTypeEnum): number[] {
    switch (clockType) {
      case ClockTypeEnum.Hour:
        return [1,2,3,4,5,6,7,8,9,10,11,12];
      case ClockTypeEnum.Min:
        return [5,10,15,20,25,30,35,40,45,50,55,0];
    }
  }
}
