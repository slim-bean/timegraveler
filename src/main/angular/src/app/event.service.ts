import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {Project} from "./project.service";

@Injectable()
export class EventService {

  constructor(public http:HttpClient) {
  }

  track(taskId:number):Observable<any> {
    const path = '/v1/task/' + taskId + '/$track';
    return this.http.post(path, null, {headers: new HttpHeaders().set('Content-Type', 'application/json')})
  }

  getEvents(projects?:Array<Project>):Observable<Event[]> {
    const path = '/v1/event';

    let queryParameters = new HttpParams();

    if (projects !== undefined && projects.length > 0) {
      for (let project of projects) {
        queryParameters = queryParameters.append('project', String(project.projectId));
      }
    }

    return this.http.get<Event[]>(path, {headers: new HttpHeaders(), params: queryParameters})
  }

  searchEvents(projects?:Array<Project>, tasks?:Array<number>, startTime?:Date, endTime?:Date, limit?:number, offset?:number):Observable<Event[]> {
    const path = 'v1/search/event';

    let startTimeLong:number = null;
    if (startTime){
      startTimeLong = startTime.getTime();
    }

    let endTimeLong:number = null;
    if (endTime){
      endTimeLong = endTime.getTime();
    }

    //Because of how angular is interpreting query params, if only one task is passed to our events-page it does not
    //create a proper array, so we test for that here and force it to be an array if it is not.
    if (tasks && !Array.isArray(tasks)) {
      tasks = [tasks];
    }

    let projArray:Array<number> = null;
    if (projects && projects.length > 0) {
      projArray = [];
      for (let project of projects) {
        projArray.push(project.projectId);
      }
    }

    var payload = {
      "startTime": startTimeLong,
      "endTime": endTimeLong,
      "limit": limit,
      "offset": offset,
      "tasks": tasks,
      "projects": projArray
    };
    console.log("Searching for events with payload: " + JSON.stringify(payload));
    return this.http.post<Event[]>(path, JSON.stringify(payload), {headers: new HttpHeaders().set('Content-Type', 'application/json')});

  }

  countEvents(projects?:Array<Project>, tasks?:Array<number>, startTime?:Date, endTime?:Date):Observable<number> {
    const path = 'v1/search/event/count';

    let startTimeLong:number = null;
    if (startTime){
      startTimeLong = startTime.getTime();
    }

    let endTimeLong:number = null;
    if (endTime){
      endTimeLong = endTime.getTime();
    }

    //Because of how angular is interpreting query params, if only one task is passed to our events-page it does not
    //create a proper array, so we test for that here and force it to be an array if it is not.
    if (tasks && !Array.isArray(tasks)) {
      tasks = [tasks];
    }

    let projArray:Array<number> = null;
    if (projects && projects.length > 0) {
      projArray = [];
      for (let project of projects) {
        projArray.push(project.projectId);
      }
    }

    let payload = {
      "startTime": startTimeLong,
      "endTime": endTimeLong,
      "tasks": tasks,
      "projects": projArray
    };
    console.log("Searching for events with payload: " + JSON.stringify(payload));
    return this.http.post<number>(path, JSON.stringify(payload), {headers: new HttpHeaders().set('Content-Type', 'application/json')});

  }

  updateEvent(currentTime:number, newEvent:Event):Observable<any> {
    const path = '/v1/event/' + currentTime;
    return this.http.put(path, JSON.stringify(newEvent), {headers: new HttpHeaders().set('Content-Type', 'application/json')})
  }

  removeEvent(eventId:number):Observable<any> {
    const path = '/v1/event/' + eventId;
    return this.http.delete(path, {headers: new HttpHeaders().set('Content-Type', 'application/json')})
  }

  getSummaryEvents(projects?:Array<Project>, tasks?:Array<number>, startTime?:number, endTime?:number):Observable<SummaryEvent[]>{
    const path = '/v1/summaryEvent';

    let queryParameters = new HttpParams();

    if (projects && projects !== undefined && projects.length > 0) {
      for (let project of projects) {
        queryParameters = queryParameters.append('project', String(project.projectId));
      }
    }

    if (tasks && tasks !== undefined && tasks.length > 0) {
      for (let task of tasks){
        queryParameters = queryParameters.append('tasks', String(task));
      }
    }

    if (startTime !== undefined) {
      queryParameters = queryParameters.set('startTime', String(startTime));
    }

    if (endTime !== undefined) {
      queryParameters = queryParameters.set('endTime', String(endTime));
    }

    return this.http.get<SummaryEvent[]>(path, {headers: new HttpHeaders(), params: queryParameters})
  }

}

export class Event {
  constructor(public taskName:string, public taskId:number, public start:number, public duration:number, public project:number, public notes:string) {
  }
}

export class SummaryEvent {
  constructor(public start:number, public finish:number, public totalMins:number, public taskId:number, public taskName:string, public project:number, public percent:number){

  }
}
