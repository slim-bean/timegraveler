import {HostListener, Injectable} from '@angular/core';
import {Subject} from "rxjs/index";

@Injectable({
  providedIn: 'root'
})
export class PwaService {
  public pwaObservable: Subject<any>;
  promptEvent;

  constructor() {
    console.log("Starting PWA Service");
    this.pwaObservable = new Subject();
  }

  receivedEvent(event){
    this.pwaObservable.next(event);
  }
}
