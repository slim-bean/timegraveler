import {Injectable} from '@angular/core';
import {Subject, Observable, BehaviorSubject} from "rxjs";
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams} from "@angular/common/http";

@Injectable()
export class ProjectService {

  private _allOpenProjects:Array<Project>;
  private _allProjects:Array<Project>;
  private _allSelectedProjects:Array<Project>;
  public projectObservable: Subject<any>;

  constructor(public http:HttpClient) {
    this._allOpenProjects = [];
    this._allProjects = [];
    this._allSelectedProjects = [];
    this.projectObservable = new Subject();
    console.log("ProjectService constructed")
  }

  get allProjects():Array<Project> {
    return this._allProjects;
  }

  get allOpenProjects():Array<Project> {
    return this._allOpenProjects;
  }

  get selectedProjects():Array<Project> {
    return this._allSelectedProjects;
  }

  set selectedProjects(value:Array<Project>) {
    let currProjectsString = '';
    for (let project of value) {
      currProjectsString = currProjectsString + "," + project.projectName
    }
    console.log("Project Service Setting New Projects: " + currProjectsString);
    this._allSelectedProjects = value;
    this.saveProjects();
    this.loadProjects();
  }

  loadProjects() {
    this.getProjects('', ['OPEN'])
      .subscribe(
        projects => {
          this._allOpenProjects = projects;
          let savedProject = localStorage.getItem("project");
          if (!savedProject){
            console.log("No saved project found in local storage, defaulting to empty list");
            this._allSelectedProjects = [];
          } else {
            console.log("Found a project saved in local storage: " + savedProject);
            try {
              let projObj: Array<number>;
              projObj = JSON.parse(savedProject);
              let tempProjects: Array<Project> = [];
              for (let projId of projObj){
                for (let project of this._allOpenProjects){
                  if (project.projectId == projId){
                    tempProjects.push(project);
                  }
                }
              }
              this._allSelectedProjects = tempProjects;
            } catch(e) {
              console.log("Couldn't parse save projects: " + e.message);
              this._allSelectedProjects = [];
            }
          }
          this.projectObservable.next();
        },
        (err: HttpErrorResponse) => {
          if (err.error instanceof Error) {
            console.log('An error occurred:', err.error.message);
            // this.toastr.error(err.error.message);
          } else {
            console.log(`Backend returned code ${err.status}, body was: ${err.error.message}`);
            // this.toastr.error(err.error.message, err.status.toString());
          }
        }
      );

    //TODO this isn't great, over time the list of projects could grow really large, but for events view and timesheet view
    //we would want to show projects even if they are closed so we need a list of all projects somewhere, or probably what we
    //really need is a lazy loading cache of projects and when you request it by ID it goes and looks it up and saves it...
    this.getProjects('', [])
      .subscribe(
        projects => {
          this._allProjects = projects;
        },
        (err: HttpErrorResponse) => {
          if (err.error instanceof Error) {
            console.log('An error occurred:', err.error.message);
            // this.toastr.error(err.error.message);
          } else {
            console.log(`Backend returned code ${err.status}, body was: ${err.error.message}`);
            // this.toastr.error(err.error.message, err.status.toString());
          }
        }
      );

  }

  saveProjects(){
    let activeProjects: Array<number> = [];
    for (let project of this._allSelectedProjects) {
      activeProjects.push(project.projectId);
    }
    localStorage.setItem("project", JSON.stringify(activeProjects));
  }

  refresh() {
    this.projectObservable.next();
  }

  reload() {
    this.loadProjects();
  }

  getProjects(filter?:string, states?:Array<string>):Observable<Project[]> {
    const path = '/v1/project';

    let queryParameters = new HttpParams();
    if (filter !== undefined){
      queryParameters = queryParameters.set('filter', encodeURIComponent(filter));
    }
    if (states !== undefined && states.length > 0) {
      for (let state of states) {
        queryParameters = queryParameters.append('state', state);
      }

    }

    return this.http.get<Project[]>(path, {headers: new HttpHeaders(), params: queryParameters});

  }

  getProjectName(id: number): string {
    //This doesn't seem terribly efficient, but javascript doesn't really have map/dictionary objects
    for (let project of this._allProjects){
      if (project.projectId == id){
        return project.projectName;
      }
    }
    return '';
  }

  getProjectById(id: number): Project {
    //This doesn't seem terribly efficient, but javascript doesn't really have map/dictionary objects
    for (let project of this._allProjects){
      if (project.projectId == id){
        return project;
      }
    }
    return null;
  }

  addProject(project: Project): Observable<any> {
    const path = '/v1/project';
    return this.http.post(path, JSON.stringify(project), {headers: new HttpHeaders().set('Content-Type', 'application/json')});
  }

  updateProject(project: Project): Observable<any> {
    const path = '/v1/project/' + project.projectId;
    return this.http.put(path, JSON.stringify(project), {headers: new HttpHeaders().set('Content-Type', 'application/json')});
  }

}


export class Project {
  constructor(public projectName:string, public projectId:number, public projectState:string){

  }
}
