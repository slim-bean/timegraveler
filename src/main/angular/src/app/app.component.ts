import {Component, HostListener, OnDestroy, OnInit} from '@angular/core';
import {Project, ProjectService} from "./project.service";
import {Subscription} from "rxjs";
import {PwaService} from "./pwa.service";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy{
  public status:{isopen:boolean} = {isopen: false};
  public projects:Array<Project>;
  public selectedProjects:Array<Project>;
  public isCollapsed: boolean = true;
  private projectSub: Subscription;
  constructor(public projectService: ProjectService, public pwaService: PwaService){

  }

  ngOnInit(){
    // this.updateProjectsList();
    // this.selectedProjects = this.projectService.project;


    this.projectSub = this.projectService.projectObservable.subscribe(() => {
      this.projects = this.projectService.allOpenProjects;
      this.selectedProjects = this.projectService.selectedProjects;
    });

    this.projectService.reload();
  }

  ngOnDestroy(): void {
    console.log("AppComponent Destory");
    this.projectSub.unsubscribe();
  }

  updateProject(){
    console.log("Updating projects");
    this.projectService.selectedProjects = this.selectedProjects;
  }

  refreshProject(){
    console.log("Triggering project refresh");
    this.projectService.refresh();
  }

  @HostListener('window:beforeinstallprompt', ['$event'])
  doSomething(event) {
    console.log("Received beforeinstallprompt: ", event);
    this.pwaService.receivedEvent(event);
  }


}
