import {Component, OnInit, ViewChild} from '@angular/core';
import {EventsComponent} from "../events/events.component";
import {TaskEvent} from "../tasks/tasks.component";

@Component({
  selector: 'app-tracker',
  templateUrl: './tracker.component.html',
  styleUrls: ['./tracker.component.css']
})
export class TrackerComponent implements OnInit {
  @ViewChild(EventsComponent)
  events:EventsComponent;

  constructor() {

  }

  ngOnInit() {

  }

  stopTracking(){
    this.events.trackTask(0)
  }

  startTrackingTask(taskEvent:TaskEvent){
    console.log('Now tracking task: ' + taskEvent.taskId);
    this.events.trackTask(taskEvent.taskId)
  }
}
