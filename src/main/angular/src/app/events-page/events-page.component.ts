import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-events-page',
  templateUrl: './events-page.component.html',
  styleUrls: ['./events-page.component.scss']
})
export class EventsPageComponent implements OnInit {

  startTime: Date;
  endTime: Date;
  tasks: Array<number>;

  constructor(private route: ActivatedRoute, private router: Router) {
    this.route.queryParams.subscribe(params => {
      if (params['startTime']){
        this.startTime = new Date(parseInt(params['startTime'],10));
        console.log("Events Page Start Time: " + this.startTime)
      }
      if (params['endTime']){
        this.endTime = new Date(parseInt(params['endTime'], 10));
        console.log("Events Page End Time: " + this.endTime)
      }
      if (params['tasks']){
        this.tasks = params['tasks'];
      }
    });
  }

  ngOnInit() {
  }

  dateChange(){
    if (this.startTime) {
      this.startTime.setHours(0);
      this.startTime.setMinutes(0);
      this.startTime.setSeconds(0);
    }
    if (this.endTime) {
      this.endTime.setHours(23);
      this.endTime.setMinutes(59);
      this.endTime.setSeconds(59);
    }
    this.router.navigate([],
      { relativeTo: this.route,
        queryParams: {"startTime": this.startTime ? this.startTime.getTime() : null,
          "endTime": this.endTime ? this.endTime.getTime() : null,
          "tasks": this.tasks ? this.tasks : null} });
  }

}
