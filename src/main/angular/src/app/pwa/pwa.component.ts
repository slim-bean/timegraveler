import {Component, OnDestroy, OnInit} from '@angular/core';
import {PwaService} from "../pwa.service";
import {Subscription} from "rxjs/Subscription";

@Component({
  selector: 'app-pwa',
  templateUrl: './pwa.component.html',
  styleUrls: ['./pwa.component.scss']
})
export class PwaComponent implements OnInit, OnDestroy {

  private pwaSub: Subscription;
  public pwaEvent;

  constructor(public pwaService: PwaService) {
    this.pwaSub = this.pwaService.pwaObservable.subscribe((event) => {
      if (event) {
        this.pwaEvent = event;
      }
    });
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.pwaSub.unsubscribe();
  }

  installPwa(): void {
    this.pwaEvent.prompt();
  }

}
