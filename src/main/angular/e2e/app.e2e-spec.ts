import { TgNgPage } from './app.po';

describe('tg-ng App', () => {
  let page: TgNgPage;

  beforeEach(() => {
    page = new TgNgPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
