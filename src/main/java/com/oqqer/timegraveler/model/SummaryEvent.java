package com.oqqer.timegraveler.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * SummaryEvent
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-03-01T02:57:57.543Z")

public class SummaryEvent   {
  @JsonProperty("start")
  private Long start = null;

  @JsonProperty("finish")
  private Long finish = null;

  @JsonProperty("totalMins")
  private Integer totalMins = null;

  @JsonProperty("taskId")
  private Long taskId = null;

  @JsonProperty("taskName")
  private String taskName = null;

  @JsonProperty("project")
  private String project = null;

  /**
   * When did the event start
   * @return start
   **/
  @ApiModelProperty(value = "When did the event start")
  public Long getStart() {
    return start;
  }

  public void setStart(Long start) {
    this.start = start;
  }


  /**
   * When did the event finish
   * @return finish
   **/
  @ApiModelProperty(value = "When did the event finish")
  public Long getFinish() {
    return finish;
  }

  public void setFinish(Long finish) {
    this.finish = finish;
  }

  public SummaryEvent totalMins(Integer totalMins) {
    this.totalMins = totalMins;
    return this;
  }

   /**
   * total minutes spent on the task
   * @return totalMins
  **/
  @ApiModelProperty(value = "total minutes spent on the task")
  public Integer getTotalMins() {
    return totalMins;
  }

  public void setTotalMins(Integer totalMins) {
    this.totalMins = totalMins;
  }

  public SummaryEvent taskId(Long taskId) {
    this.taskId = taskId;
    return this;
  }

   /**
   * What task to start tracking
   * @return taskId
  **/
  @ApiModelProperty(value = "What task to start tracking")
  public Long getTaskId() {
    return taskId;
  }

  public void setTaskId(Long taskId) {
    this.taskId = taskId;
  }

  public SummaryEvent taskName(String taskName) {
    this.taskName = taskName;
    return this;
  }

   /**
   * The name of the task
   * @return taskName
  **/
  @ApiModelProperty(value = "The name of the task")
  public String getTaskName() {
    return taskName;
  }

  public void setTaskName(String taskName) {
    this.taskName = taskName;
  }

  public SummaryEvent project(String project) {
    this.project = project;
    return this;
  }

   /**
   * What project is this task associated to
   * @return project
  **/
  @ApiModelProperty(value = "What project is this task associated to")
  public String getProject() {
    return project;
  }

  public void setProject(String project) {
    this.project = project;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SummaryEvent summaryEvent = (SummaryEvent) o;
    return Objects.equals(this.totalMins, summaryEvent.totalMins) &&
        Objects.equals(this.taskId, summaryEvent.taskId) &&
        Objects.equals(this.taskName, summaryEvent.taskName) &&
        Objects.equals(this.project, summaryEvent.project);
  }

  @Override
  public int hashCode() {
    return Objects.hash(totalMins, taskId, taskName, project);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SummaryEvent {\n");
    sb.append("    totalMins: ").append(toIndentedString(totalMins)).append("\n");
    sb.append("    taskId: ").append(toIndentedString(taskId)).append("\n");
    sb.append("    taskName: ").append(toIndentedString(taskName)).append("\n");
    sb.append("    project: ").append(toIndentedString(project)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

