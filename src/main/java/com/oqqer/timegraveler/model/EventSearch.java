package com.oqqer.timegraveler.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class EventSearch {

    @JsonProperty("startTime")
    private Long start = null;

    @JsonProperty("endTime")
    private Long finish = null;

    @JsonProperty("tasks")
    private List<Integer> tasks = null;

    @JsonProperty("projects")
    private List<Integer> projects = null;

    @JsonProperty("limit")
    private Integer limit = null;

    @JsonProperty("offset")
    private Integer offset = null;

    public Long getStart() {
        return start;
    }

    public void setStart(Long start) {
        this.start = start;
    }

    public Long getFinish() {
        return finish;
    }

    public void setFinish(Long finish) {
        this.finish = finish;
    }

    public List<Integer> getTasks() {
        return tasks;
    }

    public void setTasks(List<Integer> tasks) {
        this.tasks = tasks;
    }

    public List<Integer> getProjects() {
        return projects;
    }

    public void setProjects(List<Integer> projects) {
        this.projects = projects;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    @Override
    public String toString() {
        return "EventSearch{" +
                "start=" + start +
                ", finish=" + finish +
                ", tasks=" + tasks +
                ", projects=" + projects +
                ", limit=" + limit +
                ", offset=" + offset +
                '}';
    }
}
