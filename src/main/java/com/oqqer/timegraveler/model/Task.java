package com.oqqer.timegraveler.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * Task
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-03-01T02:57:57.543Z")

public class Task   {
  @JsonProperty("taskId")
  private Integer taskId = null;

  @JsonProperty("taskName")
  private String taskName = null;

  @JsonProperty("project")
  private Integer project = null;

  @JsonProperty("estimate")
  private Integer estimate = null;

  @JsonProperty("rank")
  private Integer rank = null;

  /**
   * the current state of the task
   */
  public enum StateEnum {
    OPEN("OPEN"),
    
    CLOSED("CLOSED"),
    
    REMOVED("REMOVED"),
    
    FUTURE("FUTURE"),

    ALL("ALL");

    private String value;

    StateEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static StateEnum fromValue(String text) {
      for (StateEnum b : StateEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("state")
  private StateEnum state = null;

  @JsonProperty("created")
  private Long created = null;

  @JsonProperty("modified")
  private Long modified = null;

  public Task taskId(Integer taskId) {
    this.taskId = taskId;
    return this;
  }

   /**
   * unique identifier
   * @return taskId
  **/
  @ApiModelProperty(value = "unique identifier")
  public Integer getTaskId() {
    return taskId;
  }

  public void setTaskId(Integer taskId) {
    this.taskId = taskId;
  }

  public Task taskName(String taskName) {
    this.taskName = taskName;
    return this;
  }

   /**
   * the task name/description
   * @return taskName
  **/
  @ApiModelProperty(value = "the task name/description")
  public String getTaskName() {
    return taskName;
  }

  public void setTaskName(String taskName) {
    this.taskName = taskName;
  }

  public Task project(Integer project) {
    this.project = project;
    return this;
  }

   /**
   * the project which the task is associated
   * @return project
  **/
  @ApiModelProperty(value = "the project which the task is associated")
  public Integer getProject() {
    return project;
  }

  public void setProject(Integer project) {
    this.project = project;
  }

  public Task estimate(Integer estimate) {
    this.estimate = estimate;
    return this;
  }

  /**
   * the estimate on how long the task will take
   * @return estimate
   **/
  @ApiModelProperty(value = "the estimate on how long the task will take")
  public Integer getEstimate() {
    return estimate;
  }

  public void setEstimate(Integer estimate) {
    this.estimate = estimate;
  }

  public Task rank(Integer rank) {
    this.rank = rank;
    return this;
  }

   /**
   * the ordinal ranking of the task
   * @return rank
  **/
  @ApiModelProperty(value = "the ordinal ranking of the task")
  public Integer getRank() {
    return rank;
  }

  public void setRank(Integer rank) {
    this.rank = rank;
  }

  public Task state(StateEnum state) {
    this.state = state;
    return this;
  }

   /**
   * the current state of the task
   * @return state
  **/
  @ApiModelProperty(value = "the current state of the task")
  public StateEnum getState() {
    return state;
  }

  public void setState(StateEnum state) {
    this.state = state;
  }

  public Task created(Long created) {
    this.created = created;
    return this;
  }

   /**
   * when was the task created
   * @return created
  **/
  @ApiModelProperty(value = "when was the task created")
  public Long getCreated() {
    return created;
  }

  public void setCreated(Long created) {
    this.created = created;
  }

  public Task modified(Long modified) {
    this.modified = modified;
    return this;
  }

   /**
   * when was the task last modified
   * @return modified
  **/
  @ApiModelProperty(value = "when was the task last modified")
  public Long getModified() {
    return modified;
  }

  public void setModified(Long modified) {
    this.modified = modified;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Task task = (Task) o;
    return Objects.equals(this.taskId, task.taskId) &&
        Objects.equals(this.taskName, task.taskName) &&
        Objects.equals(this.project, task.project) &&
        Objects.equals(this.estimate, task.estimate) &&
        Objects.equals(this.rank, task.rank) &&
        Objects.equals(this.state, task.state) &&
        Objects.equals(this.created, task.created) &&
        Objects.equals(this.modified, task.modified);
  }

  @Override
  public int hashCode() {
    return Objects.hash(taskId, taskName, project, estimate, rank, state, created, modified);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Task {\n");

    sb.append("    taskId: ").append(toIndentedString(taskId)).append("\n");
    sb.append("    taskName: ").append(toIndentedString(taskName)).append("\n");
    sb.append("    project: ").append(toIndentedString(project)).append("\n");
    sb.append("    estimate: ").append(toIndentedString(estimate)).append("\n");
    sb.append("    rank: ").append(toIndentedString(rank)).append("\n");
    sb.append("    state: ").append(toIndentedString(state)).append("\n");
    sb.append("    created: ").append(toIndentedString(created)).append("\n");
    sb.append("    modified: ").append(toIndentedString(modified)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

