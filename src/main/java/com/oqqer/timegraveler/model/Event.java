package com.oqqer.timegraveler.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * Event
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-03-01T02:57:57.543Z")

public class Event   {
  @JsonProperty("start")
  private Long start = null;

  @JsonProperty("finish")
  private Long finish = null;

  @JsonProperty("taskId")
  private Integer taskId = null;

  @JsonProperty("taskName")
  private String taskName = null;

  @JsonProperty("duration")
  private Integer duration = null;

  @JsonProperty("project")
  private String project = null;

  @JsonProperty("notes")
  private String notes = null;

  public Event start(Long start) {
    this.start = start;
    return this;
  }

   /**
   * When did the event start
   * @return start
  **/
  @ApiModelProperty(value = "When did the event start")
  public Long getStart() {
    return start;
  }

  public void setStart(Long start) {
    this.start = start;
  }

  public Event finish(Long finish) {
    this.finish = finish;
    return this;
  }

   /**
   * When did the event finish
   * @return finish
  **/
  @ApiModelProperty(value = "When did the event finish")
  public Long getFinish() {
    return finish;
  }

  public void setFinish(Long finish) {
    this.finish = finish;
  }

  public Event taskId(Integer taskId) {
    this.taskId = taskId;
    return this;
  }

   /**
   * What task to start tracking
   * @return taskId
  **/
  @ApiModelProperty(value = "What task to start tracking")
  public Integer getTaskId() {
    return taskId;
  }

  public void setTaskId(Integer taskId) {
    this.taskId = taskId;
  }

  public Event taskName(String taskName) {
    this.taskName = taskName;
    return this;
  }

   /**
   * The name of the task
   * @return taskName
  **/
  @ApiModelProperty(value = "The name of the task")
  public String getTaskName() {
    return taskName;
  }

  public void setTaskName(String taskName) {
    this.taskName = taskName;
  }

  public Event duration(Integer duration) {
    this.duration = duration;
    return this;
  }

   /**
   * How long did the task last in minutes
   * @return duration
  **/
  @ApiModelProperty(value = "How long did the task last in minutes")
  public Integer getDuration() {
    return duration;
  }

  public void setDuration(Integer duration) {
    this.duration = duration;
  }

  public Event project(String project) {
    this.project = project;
    return this;
  }

   /**
   * What project is this task associated to
   * @return project
  **/
  @ApiModelProperty(value = "What project is this task associated to")
  public String getProject() {
    return project;
  }

  public void setProject(String project) {
    this.project = project;
  }

  @ApiModelProperty(value = "Finer details about this event")
  public String getNotes() {
    return notes;
  }

  public void setNotes(String notes) {
    this.notes = notes;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Event event = (Event) o;
    return Objects.equals(this.start, event.start) &&
            Objects.equals(this.finish, event.finish) &&
            Objects.equals(this.taskId, event.taskId) &&
            Objects.equals(this.taskName, event.taskName) &&
            Objects.equals(this.duration, event.duration) &&
            Objects.equals(this.project, event.project) &&
            Objects.equals(this.notes, event.notes);
  }

  @Override
  public int hashCode() {
    return Objects.hash(start, finish, taskId, taskName, duration, project, notes);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Event {\n");

    sb.append("    start: ").append(toIndentedString(start)).append("\n");
    sb.append("    finish: ").append(toIndentedString(finish)).append("\n");
    sb.append("    taskId: ").append(toIndentedString(taskId)).append("\n");
    sb.append("    taskName: ").append(toIndentedString(taskName)).append("\n");
    sb.append("    duration: ").append(toIndentedString(duration)).append("\n");
    sb.append("    project: ").append(toIndentedString(project)).append("\n");
    sb.append("    notes: ").append(toIndentedString(notes)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

