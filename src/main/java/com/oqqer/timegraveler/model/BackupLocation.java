package com.oqqer.timegraveler.model;

import io.swagger.annotations.ApiModelProperty;

/**
 * Created by user on 5/9/17.
 */
public class BackupLocation {

    private String directory;

    private Boolean compress = true;

    @ApiModelProperty(value = "the path to where the backup should be saved, must be a directory")
    public String getDirectory() {
        return directory;
    }

    public void setDirectory(String directory) {
        this.directory = directory;
    }

    @ApiModelProperty(value = "If the backup should be saved as individual files or a compressed tar.gz, defaults true")
    public Boolean getCompress() {
        return compress;
    }

    public void setCompress(Boolean compress) {
        this.compress = compress;
    }
}
