package com.oqqer.timegraveler.model.entities;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QTask is a Querydsl query type for TaskEntity
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QTask extends com.querydsl.sql.RelationalPathBase<TaskEntity> {

    private static final long serialVersionUID = -1903466046;

    public static final QTask task = new QTask("TASK");

    public final NumberPath<Long> created = createNumber("created", Long.class);

    public final NumberPath<Integer> estimate = createNumber("estimate", Integer.class);

    public final NumberPath<Long> modified = createNumber("modified", Long.class);

    public final NumberPath<Integer> project = createNumber("project", Integer.class);

    public final NumberPath<Integer> rank = createNumber("rank", Integer.class);

    public final StringPath state = createString("state");

    public final NumberPath<Integer> taskId = createNumber("taskId", Integer.class);

    public final StringPath taskName = createString("taskName");

    public final com.querydsl.sql.PrimaryKey<TaskEntity> sysPk10202 = createPrimaryKey(taskId);

    public QTask(String variable) {
        super(TaskEntity.class, forVariable(variable), "TG", "TASK");
        addMetadata();
    }

    public QTask(String variable, String schema, String table) {
        super(TaskEntity.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QTask(String variable, String schema) {
        super(TaskEntity.class, forVariable(variable), schema, "TASK");
        addMetadata();
    }

    public QTask(Path<? extends TaskEntity> path) {
        super(path.getType(), path.getMetadata(), "TG", "TASK");
        addMetadata();
    }

    public QTask(PathMetadata metadata) {
        super(TaskEntity.class, metadata, "TG", "TASK");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(created, ColumnMetadata.named("CREATED").withIndex(7).ofType(Types.BIGINT).withSize(64).notNull());
        addMetadata(estimate, ColumnMetadata.named("ESTIMATE").withIndex(4).ofType(Types.INTEGER).withSize(32).notNull());
        addMetadata(modified, ColumnMetadata.named("MODIFIED").withIndex(8).ofType(Types.BIGINT).withSize(64).notNull());
        addMetadata(project, ColumnMetadata.named("PROJECT").withIndex(3).ofType(Types.INTEGER).withSize(32).notNull());
        addMetadata(rank, ColumnMetadata.named("RANK").withIndex(5).ofType(Types.INTEGER).withSize(32).notNull());
        addMetadata(state, ColumnMetadata.named("STATE").withIndex(6).ofType(Types.VARCHAR).withSize(50).notNull());
        addMetadata(taskId, ColumnMetadata.named("TASK_ID").withIndex(1).ofType(Types.INTEGER).withSize(32).notNull());
        addMetadata(taskName, ColumnMetadata.named("TASK_NAME").withIndex(2).ofType(Types.VARCHAR).withSize(2000).notNull());
    }

}

