package com.oqqer.timegraveler.model.entities;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QProjects is a Querydsl query type for ProjectsEntity
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QProjects extends com.querydsl.sql.RelationalPathBase<ProjectsEntity> {

    private static final long serialVersionUID = -1450118153;

    public static final QProjects projects = new QProjects("PROJECTS");

    public final NumberPath<Integer> projectId = createNumber("projectId", Integer.class);

    public final StringPath projectName = createString("projectName");

    public final StringPath projectState = createString("projectState");

    public final com.querydsl.sql.PrimaryKey<ProjectsEntity> sysPk10122 = createPrimaryKey(projectId);

    public QProjects(String variable) {
        super(ProjectsEntity.class, forVariable(variable), "TG", "PROJECTS");
        addMetadata();
    }

    public QProjects(String variable, String schema, String table) {
        super(ProjectsEntity.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QProjects(String variable, String schema) {
        super(ProjectsEntity.class, forVariable(variable), schema, "PROJECTS");
        addMetadata();
    }

    public QProjects(Path<? extends ProjectsEntity> path) {
        super(path.getType(), path.getMetadata(), "TG", "PROJECTS");
        addMetadata();
    }

    public QProjects(PathMetadata metadata) {
        super(ProjectsEntity.class, metadata, "TG", "PROJECTS");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(projectId, ColumnMetadata.named("PROJECT_ID").withIndex(1).ofType(Types.INTEGER).withSize(32).notNull());
        addMetadata(projectName, ColumnMetadata.named("PROJECT_NAME").withIndex(2).ofType(Types.VARCHAR).withSize(50).notNull());
        addMetadata(projectState, ColumnMetadata.named("PROJECT_STATE").withIndex(3).ofType(Types.VARCHAR).withSize(50).notNull());
    }

}

