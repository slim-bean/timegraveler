package com.oqqer.timegraveler.model.entities;

import javax.annotation.Generated;

/**
 * TaskAuditEntity is a Querydsl bean type
 */
@Generated("com.querydsl.codegen.BeanSerializer")
public class TaskAuditEntity {

    private Long created;

    private Integer estimate;

    private Long modified;

    private Integer project;

    private String state;

    private Integer taskId;

    private String taskName;

    public Long getCreated() {
        return created;
    }

    public void setCreated(Long created) {
        this.created = created;
    }

    public Integer getEstimate() {
        return estimate;
    }

    public void setEstimate(Integer estimate) {
        this.estimate = estimate;
    }

    public Long getModified() {
        return modified;
    }

    public void setModified(Long modified) {
        this.modified = modified;
    }

    public Integer getProject() {
        return project;
    }

    public void setProject(Integer project) {
        this.project = project;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

}

