package com.oqqer.timegraveler.model.entities;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QVEvent is a Querydsl query type for VEventEntity
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QVEvent extends com.querydsl.sql.RelationalPathBase<VEventEntity> {

    private static final long serialVersionUID = -746484159;

    public static final QVEvent vEvent = new QVEvent("V_EVENT");

    public final NumberPath<Long> durationMin = createNumber("durationMin", Long.class);

    public final NumberPath<Integer> estimate = createNumber("estimate", Integer.class);

    public final NumberPath<Long> finish = createNumber("finish", Long.class);

    public final StringPath notes = createString("notes");

    public final NumberPath<Integer> project = createNumber("project", Integer.class);

    public final NumberPath<Long> start = createNumber("start", Long.class);

    public final NumberPath<Integer> taskId = createNumber("taskId", Integer.class);

    public final StringPath taskName = createString("taskName");

    public QVEvent(String variable) {
        super(VEventEntity.class, forVariable(variable), "TG", "V_EVENT");
        addMetadata();
    }

    public QVEvent(String variable, String schema, String table) {
        super(VEventEntity.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QVEvent(String variable, String schema) {
        super(VEventEntity.class, forVariable(variable), schema, "V_EVENT");
        addMetadata();
    }

    public QVEvent(Path<? extends VEventEntity> path) {
        super(path.getType(), path.getMetadata(), "TG", "V_EVENT");
        addMetadata();
    }

    public QVEvent(PathMetadata metadata) {
        super(VEventEntity.class, metadata, "TG", "V_EVENT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(durationMin, ColumnMetadata.named("DURATION_MIN").withIndex(3).ofType(Types.BIGINT).withSize(64));
        addMetadata(estimate, ColumnMetadata.named("ESTIMATE").withIndex(7).ofType(Types.INTEGER).withSize(32));
        addMetadata(finish, ColumnMetadata.named("FINISH").withIndex(2).ofType(Types.BIGINT).withSize(64));
        addMetadata(notes, ColumnMetadata.named("NOTES").withIndex(8).ofType(Types.VARCHAR).withSize(1000));
        addMetadata(project, ColumnMetadata.named("PROJECT").withIndex(6).ofType(Types.INTEGER).withSize(32));
        addMetadata(start, ColumnMetadata.named("START").withIndex(1).ofType(Types.BIGINT).withSize(64));
        addMetadata(taskId, ColumnMetadata.named("TASK_ID").withIndex(4).ofType(Types.INTEGER).withSize(32));
        addMetadata(taskName, ColumnMetadata.named("TASK_NAME").withIndex(5).ofType(Types.VARCHAR).withSize(2000));
    }

}

