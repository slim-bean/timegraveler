package com.oqqer.timegraveler.model.entities;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QEventsAudit is a Querydsl query type for EventsAuditEntity
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QEventsAudit extends com.querydsl.sql.RelationalPathBase<EventsAuditEntity> {

    private static final long serialVersionUID = 304168843;

    public static final QEventsAudit eventsAudit = new QEventsAudit("EVENTS_AUDIT");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<Long> modified = createNumber("modified", Long.class);

    public final StringPath notes = createString("notes");

    public final NumberPath<Long> startTime = createNumber("startTime", Long.class);

    public final NumberPath<Integer> taskId = createNumber("taskId", Integer.class);

    public final com.querydsl.sql.PrimaryKey<EventsAuditEntity> sysPk10156 = createPrimaryKey(id, modified);

    public QEventsAudit(String variable) {
        super(EventsAuditEntity.class, forVariable(variable), "TG", "EVENTS_AUDIT");
        addMetadata();
    }

    public QEventsAudit(String variable, String schema, String table) {
        super(EventsAuditEntity.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QEventsAudit(String variable, String schema) {
        super(EventsAuditEntity.class, forVariable(variable), schema, "EVENTS_AUDIT");
        addMetadata();
    }

    public QEventsAudit(Path<? extends EventsAuditEntity> path) {
        super(path.getType(), path.getMetadata(), "TG", "EVENTS_AUDIT");
        addMetadata();
    }

    public QEventsAudit(PathMetadata metadata) {
        super(EventsAuditEntity.class, metadata, "TG", "EVENTS_AUDIT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(id, ColumnMetadata.named("ID").withIndex(1).ofType(Types.BIGINT).withSize(64).notNull());
        addMetadata(modified, ColumnMetadata.named("MODIFIED").withIndex(5).ofType(Types.BIGINT).withSize(64).notNull());
        addMetadata(notes, ColumnMetadata.named("NOTES").withIndex(4).ofType(Types.VARCHAR).withSize(1000));
        addMetadata(startTime, ColumnMetadata.named("START_TIME").withIndex(2).ofType(Types.BIGINT).withSize(64));
        addMetadata(taskId, ColumnMetadata.named("TASK_ID").withIndex(3).ofType(Types.INTEGER).withSize(32).notNull());
    }

}

