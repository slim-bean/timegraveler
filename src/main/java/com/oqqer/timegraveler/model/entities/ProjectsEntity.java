package com.oqqer.timegraveler.model.entities;

import javax.annotation.Generated;

/**
 * ProjectsEntity is a Querydsl bean type
 */
@Generated("com.querydsl.codegen.BeanSerializer")
public class ProjectsEntity {

    private Integer projectId;

    private String projectName;

    private String projectState;

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectState() {
        return projectState;
    }

    public void setProjectState(String projectState) {
        this.projectState = projectState;
    }

}

