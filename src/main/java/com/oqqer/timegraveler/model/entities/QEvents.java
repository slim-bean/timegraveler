package com.oqqer.timegraveler.model.entities;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QEvents is a Querydsl query type for EventsEntity
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QEvents extends com.querydsl.sql.RelationalPathBase<EventsEntity> {

    private static final long serialVersionUID = 140771286;

    public static final QEvents events = new QEvents("EVENTS");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath notes = createString("notes");

    public final NumberPath<Long> startTime = createNumber("startTime", Long.class);

    public final NumberPath<Integer> taskId = createNumber("taskId", Integer.class);

    public final com.querydsl.sql.PrimaryKey<EventsEntity> sysPk10145 = createPrimaryKey(id);

    public QEvents(String variable) {
        super(EventsEntity.class, forVariable(variable), "TG", "EVENTS");
        addMetadata();
    }

    public QEvents(String variable, String schema, String table) {
        super(EventsEntity.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QEvents(String variable, String schema) {
        super(EventsEntity.class, forVariable(variable), schema, "EVENTS");
        addMetadata();
    }

    public QEvents(Path<? extends EventsEntity> path) {
        super(path.getType(), path.getMetadata(), "TG", "EVENTS");
        addMetadata();
    }

    public QEvents(PathMetadata metadata) {
        super(EventsEntity.class, metadata, "TG", "EVENTS");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(id, ColumnMetadata.named("ID").withIndex(1).ofType(Types.BIGINT).withSize(64).notNull());
        addMetadata(notes, ColumnMetadata.named("NOTES").withIndex(4).ofType(Types.VARCHAR).withSize(1000));
        addMetadata(startTime, ColumnMetadata.named("START_TIME").withIndex(2).ofType(Types.BIGINT).withSize(64).notNull());
        addMetadata(taskId, ColumnMetadata.named("TASK_ID").withIndex(3).ofType(Types.INTEGER).withSize(32).notNull());
    }

}

