package com.oqqer.timegraveler.model.entities;

import javax.annotation.Generated;

/**
 * EventsAuditEntity is a Querydsl bean type
 */
@Generated("com.querydsl.codegen.BeanSerializer")
public class EventsAuditEntity {

    private Long id;

    private Long modified;

    private String notes;

    private Long startTime;

    private Integer taskId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getModified() {
        return modified;
    }

    public void setModified(Long modified) {
        this.modified = modified;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

}

