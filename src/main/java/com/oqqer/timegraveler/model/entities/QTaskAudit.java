package com.oqqer.timegraveler.model.entities;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;




/**
 * QTaskAudit is a Querydsl query type for TaskAuditEntity
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class QTaskAudit extends com.querydsl.sql.RelationalPathBase<TaskAuditEntity> {

    private static final long serialVersionUID = 199960863;

    public static final QTaskAudit taskAudit = new QTaskAudit("TASK_AUDIT");

    public final NumberPath<Long> created = createNumber("created", Long.class);

    public final NumberPath<Integer> estimate = createNumber("estimate", Integer.class);

    public final NumberPath<Long> modified = createNumber("modified", Long.class);

    public final NumberPath<Integer> project = createNumber("project", Integer.class);

    public final StringPath state = createString("state");

    public final NumberPath<Integer> taskId = createNumber("taskId", Integer.class);

    public final StringPath taskName = createString("taskName");

    public final com.querydsl.sql.PrimaryKey<TaskAuditEntity> sysPk10226 = createPrimaryKey(taskId, modified);

    public QTaskAudit(String variable) {
        super(TaskAuditEntity.class, forVariable(variable), "TG", "TASK_AUDIT");
        addMetadata();
    }

    public QTaskAudit(String variable, String schema, String table) {
        super(TaskAuditEntity.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public QTaskAudit(String variable, String schema) {
        super(TaskAuditEntity.class, forVariable(variable), schema, "TASK_AUDIT");
        addMetadata();
    }

    public QTaskAudit(Path<? extends TaskAuditEntity> path) {
        super(path.getType(), path.getMetadata(), "TG", "TASK_AUDIT");
        addMetadata();
    }

    public QTaskAudit(PathMetadata metadata) {
        super(TaskAuditEntity.class, metadata, "TG", "TASK_AUDIT");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(created, ColumnMetadata.named("CREATED").withIndex(6).ofType(Types.BIGINT).withSize(64).notNull());
        addMetadata(estimate, ColumnMetadata.named("ESTIMATE").withIndex(4).ofType(Types.INTEGER).withSize(32).notNull());
        addMetadata(modified, ColumnMetadata.named("MODIFIED").withIndex(7).ofType(Types.BIGINT).withSize(64).notNull());
        addMetadata(project, ColumnMetadata.named("PROJECT").withIndex(3).ofType(Types.INTEGER).withSize(32).notNull());
        addMetadata(state, ColumnMetadata.named("STATE").withIndex(5).ofType(Types.VARCHAR).withSize(50).notNull());
        addMetadata(taskId, ColumnMetadata.named("TASK_ID").withIndex(1).ofType(Types.INTEGER).withSize(32).notNull());
        addMetadata(taskName, ColumnMetadata.named("TASK_NAME").withIndex(2).ofType(Types.VARCHAR).withSize(2000).notNull());
    }

}

