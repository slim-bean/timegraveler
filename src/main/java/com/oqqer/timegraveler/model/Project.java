package com.oqqer.timegraveler.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

public class Project {
    @JsonProperty("projectId")
    private Integer projectId = null;

    @JsonProperty("projectName")
    private String projectName = null;

    /**
     * the current projectState of the project
     */
    public enum StateEnum {
        OPEN("OPEN"),

        CLOSED("CLOSED");

        private String value;

        StateEnum(String value) {
            this.value = value;
        }

        @Override
        @JsonValue
        public String toString() {
            return String.valueOf(value);
        }

        @JsonCreator
        public static StateEnum fromValue(String text) {
            for (StateEnum b : StateEnum.values()) {
                if (String.valueOf(b.value).equals(text)) {
                    return b;
                }
            }
            return null;
        }
    }

    @JsonProperty("projectState")
    private StateEnum projectState = null;


    @ApiModelProperty(value = "unique identifier")
    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    @ApiModelProperty(value = "project name")
    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    @ApiModelProperty(value = "current state of the project")
    public StateEnum getProjectState() {
        return projectState;
    }

    public void setProjectState(StateEnum projectState) {
        this.projectState = projectState;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Project project = (Project) o;
        return Objects.equals(projectId, project.projectId) &&
                Objects.equals(projectName, project.projectName) &&
                projectState == project.projectState;
    }

    @Override
    public int hashCode() {

        return Objects.hash(projectId, projectName, projectState);
    }

    @Override
    public String toString() {
        return "Project{" +
                "projectId=" + projectId +
                ", projectName='" + projectName + '\'' +
                ", projectState=" + projectState +
                '}';
    }
}
