package com.oqqer.timegraveler.configuration;

import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

/**
 * Created by user on 5/11/17.
 */
@Configuration
public class Database {


    @Bean
    @Primary
    @ConfigurationProperties(prefix="spring.datasource")
    public DataSource primaryDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "admin")
    @ConfigurationProperties(prefix="spring.datasource.admin")
    public DataSource secondaryDataSource() {
        return DataSourceBuilder.create().build();
    }


}
