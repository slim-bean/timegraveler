package com.oqqer.timegraveler.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.InternalResourceView;

/**
 * Home redirection to swagger api documentation 
 */
@Controller
public class HomeController {
	private static final Logger log = LoggerFactory.getLogger(HomeController.class);

//	@RequestMapping(value = "/swagger")
//	public String index() {
//		System.out.println("swagger-ui.html");
//		return "redirect:swagger-ui.html";
//	}

	@RequestMapping(value = "{path:[^\\.]*}")
	public View redirect() {
		// Forward to home page so that route is preserved.
		return new InternalResourceView("/index.html");
	}
}
