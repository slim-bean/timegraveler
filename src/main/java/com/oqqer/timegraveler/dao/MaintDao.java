package com.oqqer.timegraveler.dao;

import com.oqqer.timegraveler.model.BackupLocation;

/**
 * Created by user on 5/9/17.
 */
public interface MaintDao {

    void backupDatabase(BackupLocation location);

}
