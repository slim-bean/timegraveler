package com.oqqer.timegraveler.dao;

import com.oqqer.timegraveler.model.Event;
import com.oqqer.timegraveler.model.EventSearch;
import com.oqqer.timegraveler.model.SummaryEvent;

import java.util.List;

/**
 * Created by user on 4/8/17.
 */

public interface EventDao {

    List<SummaryEvent> getSummaryEvents(List<Integer> projects, List<Integer> tasks, long startTime, long endTime);

    List<Event> getEvents(List<Integer> projects, Integer limit);

    List<Event> searchEvents(EventSearch eventSearch);

    Integer countEvents(EventSearch eventSearch);

    void initiateEvent(int taskId);

    void updateEvent(Event oldEvent, Event newEvent);

    void deleteEvent(Event event);

}
