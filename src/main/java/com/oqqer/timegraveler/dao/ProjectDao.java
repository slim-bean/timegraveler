package com.oqqer.timegraveler.dao;

import com.oqqer.timegraveler.model.Project;

import java.util.List;

public interface ProjectDao {

    List<Project> getProjects(String filter, List<Project.StateEnum> states);

    void addProject(Project project);

    void updateProject(Project project);

}
