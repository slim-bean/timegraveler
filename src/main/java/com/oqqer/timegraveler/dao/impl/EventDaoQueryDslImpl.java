package com.oqqer.timegraveler.dao.impl;

import com.oqqer.timegraveler.dao.EventDao;
import com.oqqer.timegraveler.model.Event;
import com.oqqer.timegraveler.model.EventSearch;
import com.oqqer.timegraveler.model.SummaryEvent;
import com.oqqer.timegraveler.model.entities.EventsEntity;
import com.oqqer.timegraveler.model.entities.QEvents;
import com.oqqer.timegraveler.model.entities.QVEvent;
import com.oqqer.timegraveler.model.entities.VEventEntity;
import com.querydsl.core.Tuple;
import com.querydsl.core.types.dsl.CaseBuilder;
import com.querydsl.sql.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by user on 5/4/17.
 */
@Service
public class EventDaoQueryDslImpl implements EventDao {
    private static final Logger log = LoggerFactory.getLogger(EventDaoQueryDslImpl.class);

    private final DataSource dataSource;
    private final SQLQueryFactory queryFactory;

    public EventDaoQueryDslImpl(DataSource dataSource) {
        this.dataSource = dataSource;
        SQLTemplates templates = HSQLDBTemplates.builder().printSchema().build();
        Configuration config = new Configuration(templates);
        queryFactory = new SQLQueryFactory(config, dataSource);
    }

    @Override
    public List<SummaryEvent> getSummaryEvents(List<Integer> projects, List<Integer> tasks, long startTime, long endTime) {

        MapSqlParameterSource params = new MapSqlParameterSource();

        String projectQuery = "1=1";
        if (projects != null && projects.size() > 0) {
            params.addValue("projects", projects);
            projectQuery = "PROJECT IN (:projects)";
        }

        String taskQuery = "1=1";
        if (tasks != null && tasks.size() > 0) {
            params.addValue("tasks", tasks);
            taskQuery = "TASK_ID IN (:tasks)";
        }


        String query = "SELECT\n" +
                "  TASK_ID,\n" +
                "  TASK_NAME,\n" +
                "  PROJECT,\n" +
                "  SUM(DURATION_MIN) AS DURATION_MIN\n" +
                "FROM (\n" +
                "       SELECT\n" +
                "         CASE WHEN START < :startTime\n" +
                "           THEN :startTime\n" +
                "         ELSE START END        AS START,\n" +
                "         CASE WHEN FINISH > :endTime\n" +
                "           THEN :endTime\n" +
                "         ELSE FINISH END       AS FINISH,\n" +
                "         CASE WHEN START < :startTime\n" +
                "           THEN (FINISH - :startTime) / (1000 * 60)\n" +
                "         WHEN FINISH > :endTime\n" +
                "           THEN (:endTime - START) / (1000 * 60)\n" +
                "         ELSE DURATION_MIN END AS DURATION_MIN,\n" +
                "         TASK_ID,\n" +
                "         TASK_NAME,\n" +
                "         PROJECT,\n" +
                "         NOTES\n" +
                "       FROM TG.V_EVENT\n" +
                "       WHERE " + projectQuery + " AND " + taskQuery + " AND FINISH > :startTime AND START < :endTime\n" +
                "     ) S1\n" +
                "GROUP BY TASK_ID, TASK_NAME, PROJECT\n" +
                "ORDER BY 4 DESC";

        NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);

        params.addValue("startTime", startTime);
        params.addValue("endTime", endTime);

        List<SummaryEvent> resultList = new ArrayList<>();
        jdbcTemplate.query(query, params, resultSet -> {
            SummaryEvent event = new SummaryEvent();
            event.setStart(startTime);
            event.setFinish(endTime);
            event.setTaskId(resultSet.getLong("TASK_ID"));
            event.setTaskName(resultSet.getString("TASK_NAME"));
            event.setProject(String.valueOf(resultSet.getInt("PROJECT")));
            event.setTotalMins(resultSet.getInt("DURATION_MIN"));
            resultList.add(event);
        });

//        QVEvent vEvent = new QVEvent("ve");
//
//        SQLQuery<Tuple> query = queryFactory
//                .select(vEvent.taskId, vEvent.taskName, vEvent.project, vEvent.durationMin.sum().as("totalMins"))
//                .from(vEvent);
//
//        query.where(vEvent.start.goe(startTime).and(vEvent.finish.loe(endTime)));
//
//        if (projects != null && projects.size() > 0) {
//            query.where(vEvent.project.in(projects));
//        }
//
//        query.groupBy(vEvent.taskId, vEvent.taskName, vEvent.project);
//
//        List<Tuple> results = query.fetch();
//
//        List<SummaryEvent> events = results.stream()
//                .map(resultItem -> {
//                    SummaryEvent event = new SummaryEvent();
//                    event.setTaskId(resultItem.get(0, Integer.class).longValue());
//                    event.setTaskName(resultItem.get(1, String.class));
//                    event.setProject(String.valueOf(resultItem.get(2, Integer.class)));
//                    event.setTotalMins(resultItem.get(3, Long.class).intValue());
//                    return event;
//                }).collect(Collectors.toList());
//
//        events = events.stream()
//                .sorted((event1, event2) -> {
//                    if (event1.getTotalMins() < event2.getTotalMins()){
//                        return 1;
//                    } else if (event1.getTotalMins() > event2.getTotalMins()){
//                        return -1;
//                    } else {
//                        return 0;
//                    }
//                }).collect(Collectors.toList());

        return resultList;
    }

    @Override
    public List<Event> getEvents(List<Integer> projects, Integer limit) {

        QVEvent vEvent = new QVEvent("ve");

        SQLQuery<VEventEntity> query = queryFactory.selectFrom(vEvent);

        if (projects != null && projects.size() > 0) {
            query.where(vEvent.project.in(projects));
        }

        if (limit != null) {
            query.limit(limit);
        }

        List<VEventEntity> vEventEntities = query
                .orderBy(vEvent.start.desc())
                .fetch();


        List<Event> events = vEventEntities.stream()
                .map(eventItem -> {
                    Event event = new Event();
                    event.setDuration(eventItem.getDurationMin() == null ? null : eventItem.getDurationMin().intValue());
                    event.setFinish(eventItem.getFinish());
                    event.setProject(String.valueOf(eventItem.getProject()));
                    event.setStart(eventItem.getStart());
                    event.setTaskId(eventItem.getTaskId());
                    event.setTaskName(eventItem.getTaskName());
                    event.setNotes(eventItem.getNotes());
                    return event;
                })
                .collect(Collectors.toList());

        return events;
    }

    @Override
    public List<Event> searchEvents(EventSearch eventSearch) {

        MapSqlParameterSource params = new MapSqlParameterSource();

        String projectQuery = "1=1";
        if (eventSearch.getProjects() != null && eventSearch.getProjects().size() > 0) {
            params.addValue("projects", eventSearch.getProjects());
            projectQuery = "PROJECT IN (:projects)";
        }

        String taskQuery = "1=1";
        if (eventSearch.getTasks() != null && eventSearch.getTasks().size() > 0) {
            params.addValue("tasks", eventSearch.getTasks());
            taskQuery = "TASK_ID IN (:tasks)";
        }

        String startQuery = "1=1";
        if (eventSearch.getStart() == null) {
            params.addValue("startTime", 0L);
        } else {
            startQuery = "FINISH > :startTime";
            params.addValue("startTime", eventSearch.getStart());
        }

        String finishQuery = "1=1";
        if (eventSearch.getFinish() == null) {
            params.addValue("endTime", Long.MAX_VALUE);
        } else {
            finishQuery = "START < :endTime";
            params.addValue("endTime", eventSearch.getFinish());
        }

        if (eventSearch.getLimit() == null) {
            params.addValue("limit", 50);
        } else {
            params.addValue("limit", eventSearch.getLimit());
        }

        if (eventSearch.getOffset() == null) {
            params.addValue("offset", 0);
        } else {
            params.addValue("offset", eventSearch.getOffset());
        }

        String query = "SELECT\n" +
                "         CASE WHEN START < :startTime\n" +
                "           THEN :startTime\n" +
                "         ELSE START END        AS START,\n" +
                "         CASE WHEN FINISH > :endTime\n" +
                "           THEN :endTime\n" +
                "         ELSE FINISH END       AS FINISH,\n" +
                "         CASE WHEN START < :startTime\n" +
                "           THEN (FINISH - :startTime) / (1000 * 60)\n" +
                "         WHEN FINISH > :endTime\n" +
                "           THEN (:endTime - START) / (1000 * 60)\n" +
                "         ELSE DURATION_MIN END AS DURATION_MIN,\n" +
                "         TASK_ID,\n" +
                "         TASK_NAME,\n" +
                "         PROJECT,\n" +
                "         NOTES\n" +
                "       FROM TG.V_EVENT\n" +
                "       WHERE " + projectQuery + " AND " + taskQuery + " AND " + startQuery + " AND " + finishQuery + " \n" +
                "       ORDER BY 1 DESC \n" +
                "       LIMIT :limit OFFSET :offset ";

        NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);

        List<Event> resultList = new ArrayList<>();
        jdbcTemplate.query(query, params, resultSet -> {
            Event event = new Event();
            event.setStart(resultSet.getLong("START"));
            event.setFinish(resultSet.getLong("FINISH"));
            event.setDuration(resultSet.getInt("DURATION_MIN"));
            event.setTaskId(resultSet.getInt("TASK_ID"));
            event.setTaskName(resultSet.getString("TASK_NAME"));
            event.setProject(String.valueOf(resultSet.getInt("PROJECT")));
            event.setNotes(resultSet.getString("NOTES"));
            resultList.add(event);
        });

        return resultList;
    }

    @Override
    public Integer countEvents(EventSearch eventSearch) {
        QVEvent vEvent = new QVEvent("ve");

        SQLQuery<VEventEntity> query = queryFactory.selectFrom(vEvent);

        if (eventSearch.getProjects() != null && eventSearch.getProjects().size() > 0) {
            query.where(vEvent.project.in(eventSearch.getProjects()));
        }

        if (eventSearch.getTasks() != null && eventSearch.getTasks().size() > 0) {
            query.where(vEvent.taskId.in(eventSearch.getTasks()));
        }

        if (eventSearch.getStart() != null) {
            query.where(vEvent.start.gt(eventSearch.getStart()));
        }

        if (eventSearch.getFinish() != null) {
            query.where(vEvent.finish.lt(eventSearch.getFinish()));
        }

        return (int) query.fetchCount();
    }

    @Override
    public void initiateEvent(int taskId) {
        QEvents eventsTable = new QEvents("event");

        EventsEntity event = new EventsEntity();
        event.setStartTime(System.currentTimeMillis());
        event.setTaskId(taskId);

        queryFactory.insert(eventsTable)
                .populate(event)
                .execute();
    }

    @Override
    public void updateEvent(Event oldEvent, Event newEvent) {

        if (oldEvent.getStart() == null) {
            throw new IllegalArgumentException("Must provide an existing event with valid startTime");
        }

        if (newEvent.getStart() == null) {
            throw new IllegalArgumentException("Must provide a valid new event startTime");
        }

        QEvents eventsTable = new QEvents("event");

        EventsEntity eventEntity = new EventsEntity();
        eventEntity.setStartTime(newEvent.getStart());
        eventEntity.setNotes(newEvent.getNotes());

        queryFactory.update(eventsTable)
                .populate(eventEntity)
                .where(eventsTable.startTime.eq(oldEvent.getStart()))
                .execute();

    }

    @Override
    public void deleteEvent(Event event) {
        if (event.getStart() == null) {
            throw new IllegalArgumentException("Must provide an existing event with valid startTime");
        }

        QEvents eventsTable = new QEvents("event");

        long rows = queryFactory.delete(eventsTable)
                .where(eventsTable.startTime.eq(event.getStart()))
                .execute();

        log.info("Deleted {} rows with startTime {}", rows, event.getStart());
    }
}
