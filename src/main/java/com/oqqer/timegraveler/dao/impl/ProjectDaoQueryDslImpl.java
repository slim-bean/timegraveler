package com.oqqer.timegraveler.dao.impl;

import com.oqqer.timegraveler.dao.ProjectDao;
import com.oqqer.timegraveler.model.Project;
import com.oqqer.timegraveler.model.entities.ProjectsEntity;
import com.oqqer.timegraveler.model.entities.QProjects;
import com.querydsl.sql.*;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProjectDaoQueryDslImpl implements ProjectDao {

    private final DataSource dataSource;
    private final SQLQueryFactory queryFactory;
    private final Configuration config;

    public ProjectDaoQueryDslImpl(DataSource dataSource) {
        this.dataSource = dataSource;
        SQLTemplates templates = HSQLDBTemplates.builder().printSchema().build();
        config = new Configuration(templates);
        queryFactory = new SQLQueryFactory(config, dataSource);
    }

    @Override
    public List<Project> getProjects(String filter, List<Project.StateEnum> states) {

        QProjects projectsTable = new QProjects("projects");

        SQLQuery<ProjectsEntity> query = queryFactory
                .selectFrom(projectsTable);

        if (filter != null) {
            query.where(projectsTable.projectName.toLowerCase().contains(filter.toLowerCase()));
        }

        if (states != null) {
            List<String> stateStrings = states.stream().map(Enum::name).collect(Collectors.toList());
            query.where(projectsTable.projectState.in(stateStrings));
        }

        List<ProjectsEntity> taskEntities = query.fetch();

        List<Project> projects = taskEntities.stream()
                .map(ProjectDaoQueryDslImpl::convert)
                .collect(Collectors.toList());

        return projects;
    }

    @Override
    public void addProject(Project project) {
        QProjects projectsTable = new QProjects("projects");

        ProjectsEntity entity = new ProjectsEntity();
        entity.setProjectName(project.getProjectName());
        entity.setProjectState(Project.StateEnum.OPEN.name());

        queryFactory.insert(projectsTable)
                .populate(entity)
                .execute();
    }

    @Override
    public void updateProject(Project project) {
        QProjects projectsTable = new QProjects("projects");

        ProjectsEntity entity = new ProjectsEntity();
        if (project.getProjectName() != null) {
            entity.setProjectName(project.getProjectName());
        }
        if (project.getProjectState() != null) {
            entity.setProjectState(project.getProjectState().name());
        }

        queryFactory.update(projectsTable)
                .populate(entity)
                .where(projectsTable.projectId.eq(project.getProjectId()))
                .execute();
    }

    private static Project convert(ProjectsEntity entity) {
        Project project = new Project();
        project.setProjectId(entity.getProjectId());
        project.setProjectName(entity.getProjectName());
        project.setProjectState(Project.StateEnum.valueOf(entity.getProjectState()));
        return project;
    }
}
