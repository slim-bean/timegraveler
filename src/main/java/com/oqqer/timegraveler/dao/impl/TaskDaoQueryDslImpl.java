package com.oqqer.timegraveler.dao.impl;

import com.oqqer.timegraveler.dao.TaskDao;
import com.oqqer.timegraveler.model.Task;
import com.oqqer.timegraveler.model.entities.QTask;
import com.oqqer.timegraveler.model.entities.TaskEntity;
import com.querydsl.sql.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by user on 5/4/17.
 */
@Service
public class TaskDaoQueryDslImpl implements TaskDao {

    private static final Logger log = LoggerFactory.getLogger(TaskDaoQueryDslImpl.class);

    private final DataSource dataSource;
    private final SQLQueryFactory queryFactory;
    private final Configuration config;
    private final Object rankLock = new Object();

    public TaskDaoQueryDslImpl(DataSource dataSource) {
        this.dataSource = dataSource;
        SQLTemplates templates = HSQLDBTemplates.builder().printSchema().build();
        config = new Configuration(templates);
        queryFactory = new SQLQueryFactory(config, dataSource);
    }

    @Override
    public List<Task> getTasks(String filter, List<Integer> projects, List<Task.StateEnum> states) {

        QTask taskTable = new QTask("task");

        SQLQuery<TaskEntity> query = queryFactory
                .selectFrom(taskTable);

        if (filter != null) {
            query.where(taskTable.taskName.toLowerCase().contains(filter.toLowerCase()));
        }

        if (projects != null) {
            query.where(taskTable.project.in(projects));
        }

        if (states != null) {
            List<String> stateStrings = states.stream().map(Enum::name).collect(Collectors.toList());
            query.where(taskTable.state.in(stateStrings));
        }

        List<TaskEntity> taskEntities = query.orderBy(taskTable.rank.desc()).fetch();

        List<Task> tasks = taskEntities.stream()
                .map(TaskDaoQueryDslImpl::convert)
                .collect(Collectors.toList());

        return tasks;
    }

    @Override
    public void addTask(Task task) {
        QTask taskTable = new QTask("task");

        TaskEntity taskEntity = new TaskEntity();
        taskEntity.setCreated(System.currentTimeMillis());
        taskEntity.setModified(System.currentTimeMillis());
        taskEntity.setProject(task.getProject());
        taskEntity.setEstimate(task.getEstimate() == null ? 0 : task.getEstimate());
        taskEntity.setTaskName(task.getTaskName());
        taskEntity.setState(Task.StateEnum.OPEN.name());

        queryFactory.insert(taskTable)
                .populate(taskEntity)
                .set(taskTable.rank, SQLExpressions.select(taskTable.rank.max().add(1)).from(taskTable))
                .execute();
    }

    @Override
    public Task getTask(Integer id) {
        QTask taskTable = new QTask("task");

        TaskEntity taskEntity = queryFactory.selectFrom(taskTable)
                .where(taskTable.taskId.eq(id))
                .fetchOne();

        if (taskEntity == null) {
            return null;
        }

        return convert(taskEntity);
    }

    @Override
    public void updateTask(Task task) {
        QTask taskTable = new QTask("task");

        if (task.getTaskId() == null) {
            throw new IllegalArgumentException("taskId cannot be null");
        }

        //By default any null values are ignored on update, so we will only update to new values
        TaskEntity taskEntity = new TaskEntity();
        taskEntity.setState(task.getState() == null ? null : task.getState().name());
        taskEntity.setTaskName(task.getTaskName());
        taskEntity.setRank(task.getRank());
        taskEntity.setProject(task.getProject());
        taskEntity.setEstimate(task.getEstimate());
        taskEntity.setModified(System.currentTimeMillis());

        queryFactory.update(taskTable)
                .populate(taskEntity)
                .where(taskTable.taskId.eq(task.getTaskId()))
                .execute();
    }

    @Override
    public void rerankTask(int taskId, int newRank) {

        //TODO I'd really prefer this were locked by a database transaction but I couldn't figure out how to get querydsl to process multiple queries in a single transaction...
        synchronized (rankLock) {
            QTask taskTable = new QTask("task");

            //Update everything that previously had a higher rank which was also less than the new rank, to have a rank one less


            queryFactory.update(taskTable).set(taskTable.rank, taskTable.rank.subtract(1))
                    .where(taskTable.rank.goe(SQLExpressions.select(taskTable.rank).from(taskTable).where(taskTable.taskId.eq(taskId)))
                            .and(taskTable.rank.loe(newRank)))
                    .execute();


            //Update everything that previously had a lower rank which was also greater than the new rank, to have a rank one greater
            queryFactory.update(taskTable).set(taskTable.rank, taskTable.rank.add(1))
                    .where(taskTable.rank.loe(SQLExpressions.select(taskTable.rank).from(taskTable).where(taskTable.taskId.eq(taskId)))
                            .and(taskTable.rank.goe(newRank)))
                    .execute();

            //Update the rank for this item
            queryFactory.update(taskTable).set(taskTable.rank, newRank)
                    .where(taskTable.taskId.eq(taskId))
                    .execute();

        }

    }

    @Override
    public void moveTaskUp(int taskId) {
        synchronized (rankLock) {
            QTask taskTable = new QTask("task");

            TaskEntity currentTask = queryFactory.selectFrom(taskTable)
                    .where(taskTable.taskId.eq(taskId))
                    .fetchOne();

            TaskEntity nextHigherTask = queryFactory.selectFrom(taskTable)
                    .where(taskTable.project.eq(currentTask.getProject())
                            .and(taskTable.rank.eq(SQLExpressions.select(taskTable.rank.min()).from(taskTable)
                                    .where(taskTable.project.eq(currentTask.getProject())
                                            .and(taskTable.rank.gt(currentTask.getRank()))
                                            .and(taskTable.state.eq(Task.StateEnum.OPEN.name()))))))
                    .fetchOne();

            if (nextHigherTask == null) {
                log.info("Task is already the highest rank, ignoring request to move up");
                return;
            }

            //Update the current task to the next higher
            queryFactory.update(taskTable)
                    .set(taskTable.rank, nextHigherTask.getRank())
                    .where(taskTable.taskId.eq(taskId))
                    .execute();

            //Update the other task to the next lower
            queryFactory.update(taskTable)
                    .set(taskTable.rank, currentTask.getRank())
                    .where(taskTable.taskId.eq(nextHigherTask.getTaskId()))
                    .execute();

        }
    }

    @Override
    public void moveTaskDown(int taskId) {
        synchronized (rankLock) {
            QTask taskTable = new QTask("task");

            TaskEntity currentTask = queryFactory.selectFrom(taskTable)
                    .where(taskTable.taskId.eq(taskId))
                    .fetchOne();

            TaskEntity nextLowerTask = queryFactory.selectFrom(taskTable)
                    .where(taskTable.project.eq(currentTask.getProject())
                            .and(taskTable.rank.eq(SQLExpressions.select(taskTable.rank.max()).from(taskTable)
                                    .where(taskTable.project.eq(currentTask.getProject())
                                            .and(taskTable.rank.lt(currentTask.getRank()))
                                            .and(taskTable.state.eq(Task.StateEnum.OPEN.name()))))))
                    .fetchOne();

            if (nextLowerTask == null) {
                log.info("Task is already the lowest rank, ignoring request to move up");
                return;
            }

            //Update the current task to the next higher
            queryFactory.update(taskTable)
                    .set(taskTable.rank, nextLowerTask.getRank())
                    .where(taskTable.taskId.eq(taskId))
                    .execute();

            //Update the other task to the next lower
            queryFactory.update(taskTable)
                    .set(taskTable.rank, currentTask.getRank())
                    .where(taskTable.taskId.eq(nextLowerTask.getTaskId()))
                    .execute();
        }

    }

    private static Task convert(TaskEntity entity) {
        Task task = new Task();
        task.setCreated(entity.getCreated());
        task.setModified(entity.getModified());
        task.setProject(entity.getProject());
        task.setEstimate(entity.getEstimate());
        task.setRank(entity.getRank());
        task.setState(Task.StateEnum.valueOf(entity.getState()));
        task.setTaskId(entity.getTaskId());
        task.setTaskName(entity.getTaskName());
        return task;
    }


}
