package com.oqqer.timegraveler.dao.impl;

import com.oqqer.timegraveler.dao.MaintDao;
import com.oqqer.timegraveler.model.BackupLocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.regex.Pattern;

/**
 * Created by user on 5/9/17.
 */
@Service
public class MaintDaoQueryDslImpl implements MaintDao{
    private static final Logger log = LoggerFactory.getLogger(MaintDaoQueryDslImpl.class);

    private final DataSource dataSource;

    public MaintDaoQueryDslImpl(@Qualifier("admin") DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void backupDatabase(BackupLocation location) {
        if (location.getDirectory() == null) {
            throw new IllegalArgumentException("Must provide a path for backup");
        }
        String path = location.getDirectory();
        if (!path.endsWith("/")) {
            path = path + "/";
        }
        if (!Pattern.matches("[a-zA-z0-9/\\-_]*", path)){
            throw new IllegalArgumentException("Backup path can only contain letters, numbers, dashes, and/or underscores");
        }
        log.info("Backing up database to: {}", path);
        String compress = "";
        if (location.getCompress() != null && !location.getCompress()) {
            compress = " AS FILES";
        }
        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement ps = conn.prepareStatement("BACKUP DATABASE TO '" + path + "' BLOCKING" + compress);
            ps.execute();
            log.info("Backup successful");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }
}
