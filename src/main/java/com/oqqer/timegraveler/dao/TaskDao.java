package com.oqqer.timegraveler.dao;

import com.oqqer.timegraveler.model.Task;

import java.util.List;

/**
 * Created by user on 4/25/17.
 */
public interface TaskDao {

    List<Task> getTasks(String filter, List<Integer> projects, List<Task.StateEnum> states);

    void addTask(Task task);

    Task getTask(Integer id);

    void updateTask(Task task);

    void rerankTask(int taskId, int newRank);

    void moveTaskUp(int taskId);

    void moveTaskDown(int taskId);
}
