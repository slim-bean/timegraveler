package com.oqqer.timegraveler.codegen;

import com.querydsl.codegen.BeanSerializer;
import com.querydsl.sql.codegen.MetaDataExporter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;


/**
 * Created by user on 4/25/17.
 */
public class QuerydslCodegen {
    private static final Logger log = LoggerFactory.getLogger(QuerydslCodegen.class);

    private static final String JDBC_DRIVER = "org.hsqldb.jdbc.JDBCDriver";
    private static final String DB_URL = "jdbc:hsqldb:hsql://localhost:9001/tg";
    private static final String USER = "TG_USER";

    public static void main(String[] args) throws Exception {

        Class.forName(JDBC_DRIVER);

        log.info("Connecting to database {}...", DB_URL);
        try (Connection conn = DriverManager.getConnection(DB_URL, USER, args[0])){
            log.info("Connected");
            log.info("Exporting Schema");
            MetaDataExporter exporter = new MetaDataExporter();
            exporter.setPackageName("com.oqqer.timegraveler.model.entities");
            exporter.setTargetFolder(new File("src/main/java"));
            exporter.setTableNamePattern("EVENTS,EVENTS_AUDIT,PROJECTS,TASK,TASK_AUDIT,V_EVENT");
            exporter.setBeanSerializer(new BeanSerializer());
            exporter.setBeanSuffix("Entity");
            exporter.setExportForeignKeys(false);
            exporter.export(conn.getMetaData());
            log.info("Finished");
        }
        log.info("Disconnected");




    }

}
