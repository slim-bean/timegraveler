package com.oqqer.timegraveler.api;

import com.oqqer.timegraveler.dao.EventDao;
import com.oqqer.timegraveler.dao.MaintDao;
import com.oqqer.timegraveler.dao.ProjectDao;
import com.oqqer.timegraveler.dao.TaskDao;
import com.oqqer.timegraveler.model.*;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;


@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-03-01T02:57:57.543Z")

@RestController
public class V1ApiController implements V1Api {

    private static final Logger log = LoggerFactory.getLogger(V1ApiController.class);

    private final EventDao eventDao;
    private final TaskDao taskDao;
    private final MaintDao maintDao;
    private final ProjectDao projectDao;

    public V1ApiController(EventDao eventDao, TaskDao taskDao, MaintDao maintDao, ProjectDao projectDao) {
        this.eventDao = eventDao;
        this.taskDao = taskDao;
        this.maintDao = maintDao;
        this.projectDao = projectDao;
    }

    public ResponseEntity<List<SummaryEvent>> v1SummaryEvent(@ApiParam(value = "filter the response by project") @RequestParam(value = "project", required = false) List<String> project,
                                                             @ApiParam(value = "filter the repsonse by task") @RequestParam(value = "tasks", required = false) List<String> tasks,
                                                             @ApiParam(value = "inclusive") @RequestParam(value = "startTime", required = true) Long startTime,
                                                             @ApiParam(value = "inclusive") @RequestParam(value = "endTime", required = true) Long endTime) {

        log.info("Fetching summary events for projects: {}, tasks {}, between {} and {}", project, tasks, startTime, endTime);

        List<Integer> projects;
        if (project == null) {
            projects = null;
        } else {
            projects = project.stream().map(projectItem -> Integer.parseInt(projectItem)).collect(Collectors.toList());
        }
        List<Integer> integerTasks;
        if (tasks == null) {
            integerTasks = null;
        } else {
            integerTasks = tasks.stream().map(taskItem -> Integer.parseInt(taskItem)).collect(Collectors.toList());
        }

        List<SummaryEvent> summaryEvents = eventDao.getSummaryEvents(projects, integerTasks, startTime, endTime);
        return new ResponseEntity<List<SummaryEvent>>(summaryEvents, HttpStatus.OK);
    }

    public ResponseEntity<Void> v1EventUpdate(@ApiParam(value = "",required=true ) @PathVariable("eventId") Long eventId,
                                              @ApiParam(value = "" ,required=true ) @RequestBody Event event) {

        // Events are keyed by their start time, so we make an oldEvent object using the path param of the current event time
        Event oldEvent = new Event();
        oldEvent.setStart(eventId);

        eventDao.updateEvent(oldEvent, event);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> v1EventDelete(@ApiParam(value = "",required=true ) @PathVariable("eventId") Long eventId) {
        Event event = new Event();
        event.setStart(eventId);
        eventDao.deleteEvent(event);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    public ResponseEntity<Void> v1EventInitiate(@ApiParam(value = "",required=true ) @PathVariable("taskId") Integer taskId) {
        eventDao.initiateEvent(taskId);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    public ResponseEntity<List<Event>> v1EventGet(@ApiParam(value = "filter the response by project") @RequestParam(value = "project", required = false) List<String> project) {

        List<Integer> projects;
        if (project == null) {
            projects = null;
        } else {
            projects = project.stream().map(projectItem -> Integer.parseInt(projectItem)).collect(Collectors.toList());
        }

        List<Event> events = eventDao.getEvents(projects, 50);

        return new ResponseEntity<List<Event>>(events, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<Event>> v1EventSearch(@ApiParam(value = "search parameters") @RequestBody EventSearch eventSearch) {
        log.info("Searching for events for search request: {}", eventSearch);
        List<Event> events = eventDao.searchEvents(eventSearch);
        return new ResponseEntity<List<Event>>(events, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Integer> v1EventSearchCount(@ApiParam(value = "search parameters") @RequestBody EventSearch eventSearch) {
        return new ResponseEntity<>(eventDao.countEvents(eventSearch), HttpStatus.OK);
    }

    public ResponseEntity<List<Task>> v1TaskGet(@ApiParam(value = "") @RequestParam(value = "filter", required = false) String filter,
                                                @ApiParam(value = "only include these projects") @RequestParam(value = "project", required = false) List<String> project,
                                                @ApiParam(value = "only return tasks with this state") @RequestParam(value = "state", required = false) List<Task.StateEnum> state) {
        List<Integer> projects;
        if (project == null) {
            projects = null;
        } else {
            projects = project.stream().map(projectItem -> Integer.parseInt(projectItem)).collect(Collectors.toList());
        }

        List<Task> tasks = taskDao.getTasks(filter, projects, state);

        return new ResponseEntity<List<Task>>(tasks, HttpStatus.OK);
    }

    public ResponseEntity<Void> v1TaskPost(@ApiParam(value = "" ,required=true ) @RequestBody Task task) {
        taskDao.addTask(task);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    public ResponseEntity<Task> v1TaskGetById(@ApiParam(value = "",required=true ) @PathVariable("taskId") Integer taskId) {
        if (taskId == null) {
            return new ResponseEntity<Task>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<Task>(taskDao.getTask(taskId), HttpStatus.OK);

    }

    public ResponseEntity<Void> v1TaskPut(@ApiParam(value = "",required=true ) @PathVariable("taskId") Integer taskId,
                                          @ApiParam(value = "" ,required=true ) @RequestBody Task task) {
        if (task.getTaskId() == null) {
            task.setTaskId(taskId);
        }
        taskDao.updateTask(task);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    public ResponseEntity<Void> v1TaskClose(@ApiParam(value = "",required=true ) @PathVariable("taskId") Integer taskId) {
        Task task = new Task();
        task.setTaskId(taskId);
        task.setState(Task.StateEnum.CLOSED);
        taskDao.updateTask(task);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    public ResponseEntity<Void> v1TaskRerank(@ApiParam(value = "",required=true ) @PathVariable("taskId") Integer taskId,
                                             @ApiParam(value = "" ,required=true ) @RequestBody Task task) {
        if (taskId == null || task.getRank() == null) {
            throw new IllegalArgumentException("Must provide the taskId and the task must have a rank");
        }
        taskDao.rerankTask(taskId, task.getRank());
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> v1TaskMoveUp(@ApiParam(value = "", required = true) @PathVariable("taskId") Integer taskId) {
        taskDao.moveTaskUp(taskId);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> v1TaskMoveDown(@ApiParam(value = "", required = true) @PathVariable("taskId") Integer taskId) {
        taskDao.moveTaskDown(taskId);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<Project>> v1ProjectGet(@RequestParam(value = "filter", required = false)String filter,
                                                      @RequestParam(value = "state", required = false)List<Project.StateEnum> states) {
        List<Project> projects = projectDao.getProjects(filter, states);
        return new ResponseEntity<List<Project>>(projects, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> v1ProjectPost(@RequestBody Project project) {
        projectDao.addProject(project);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> v1ProjectPut(@PathVariable("projectId") Integer projectId,
                                             @RequestBody Project project) {
        project.setProjectId(projectId);
        projectDao.updateProject(project);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> v1DatabaseBackup(@ApiParam(value = "", required = true) @RequestBody BackupLocation location) {
        maintDao.backupDatabase(location);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
