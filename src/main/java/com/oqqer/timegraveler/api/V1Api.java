package com.oqqer.timegraveler.api;

import com.oqqer.timegraveler.model.*;
import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-03-01T02:57:57.543Z")

@Api(value = "v1", description = "the v1 API")
public interface V1Api {

    @ApiOperation(value = "Pull a list of events summarized by task", notes = "a summary of time!", response = SummaryEvent.class, responseContainer = "List", tags={ "event_controller", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "An array of SummaryEvents", response = SummaryEvent.class),
        @ApiResponse(code = 200, message = "Unexpected Error", response = SummaryEvent.class) })
    @RequestMapping(value = "/v1/summaryEvent",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    ResponseEntity<List<SummaryEvent>> v1SummaryEvent(@ApiParam(value = "filter the response by project") @RequestParam(value = "project", required = false) List<String> project,
                                                      @ApiParam(value = "filter the repsonse by task") @RequestParam(value = "tasks", required = false) List<String> tasks,
                                                      @ApiParam(value = "inclusive") @RequestParam(value = "startTime", required = false) Long startTime,
                                                      @ApiParam(value = "inclusive") @RequestParam(value = "endTime", required = false) Long endTime);

    @ApiOperation(value = "Start tracking a task", notes = "Tells the database to add an event to the event table for this task", response = Void.class, tags={ "task_controller", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Started Tracking Task", response = Void.class),
            @ApiResponse(code = 200, message = "Unexpected Error", response = Void.class) })
    @RequestMapping(value = "/v1/task/{taskId}/$track",
            produces = { "application/json" },
            method = RequestMethod.POST)
    ResponseEntity<Void> v1EventInitiate(@ApiParam(value = "", required = true) @PathVariable("taskId") Integer taskId);

    @ApiOperation(value = "Update an event", notes = "Update an event", response = Void.class, tags={ "event_controller", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Update Success", response = Void.class),
        @ApiResponse(code = 200, message = "Unexpected Error", response = Void.class) })
    @RequestMapping(value = "/v1/event/{eventId}",
        produces = { "application/json" }, 
        method = RequestMethod.PUT)
    ResponseEntity<Void> v1EventUpdate(@ApiParam(value = "", required = true) @PathVariable("eventId") Long eventId,
                                       @ApiParam(value = "", required = true) @RequestBody Event event);

    @ApiOperation(value = "Delete an event", notes = "Delete an event", response = Void.class, tags={ "event_controller", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Remove Success", response = Void.class),
            @ApiResponse(code = 200, message = "Unexpected Error", response = Void.class) })
    @RequestMapping(value = "/v1/event/{eventId}",
            produces = { "application/json" },
            method = RequestMethod.DELETE)
    ResponseEntity<Void> v1EventDelete(@ApiParam(value = "", required = true) @PathVariable("eventId") Long eventId);

    @ApiOperation(value = "Get all events", notes = "Returns a list of all events in order starting with the most recent", response = Event.class, responseContainer = "List", tags={ "event_controller", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "An array of events", response = Event.class),
        @ApiResponse(code = 200, message = "Unexpected Error", response = Event.class) })
    @RequestMapping(value = "/v1/event",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    ResponseEntity<List<Event>> v1EventGet(@ApiParam(value = "filter the response by project") @RequestParam(value = "project", required = false) List<String> project);


    @ApiOperation(value = "Get events", notes = "Returns a list of events based on search criteriea", response = Event.class, responseContainer = "List", tags={ "search_controller", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "An array of events", response = Event.class),
            @ApiResponse(code = 200, message = "Unexpected Error", response = Event.class) })
    @RequestMapping(value = "/v1/search/event",
            produces = { "application/json" },
            method = RequestMethod.POST)
    ResponseEntity<List<Event>> v1EventSearch(@ApiParam(value = "search parameters") @RequestBody EventSearch eventSearch);

    @ApiOperation(value = "Get events", notes = "Returns a count of events based on search criteriea", response = Integer.class, tags={ "search_controller", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "An array of events", response = Integer.class),
            @ApiResponse(code = 200, message = "Unexpected Error", response = Integer.class) })
    @RequestMapping(value = "/v1/search/event/count",
            produces = { "application/json" },
            method = RequestMethod.POST)
    ResponseEntity<Integer> v1EventSearchCount(@ApiParam(value = "search parameters") @RequestBody EventSearch eventSearch);


    @ApiOperation(value = "Get All Task", notes = "Get All Tasks", response = Task.class, responseContainer = "List", tags={ "task_controller", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "An array of products", response = Task.class),
        @ApiResponse(code = 200, message = "Unexpected error", response = Task.class) })
    @RequestMapping(value = "/v1/task",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    ResponseEntity<List<Task>> v1TaskGet(@ApiParam(value = "") @RequestParam(value = "filter", required = false) String filter,
                                         @ApiParam(value = "only include these projects") @RequestParam(value = "project", required = false) List<String> project,
                                         @ApiParam(value = "only return tasks with this state") @RequestParam(value = "state", required = false) List<Task.StateEnum> state);


    @ApiOperation(value = "Add a task", notes = "Add a task", response = Void.class, tags={ "task_controller", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Success", response = Void.class),
        @ApiResponse(code = 200, message = "Unexpected error", response = Void.class) })
    @RequestMapping(value = "/v1/task",
        produces = { "application/json" }, 
        method = RequestMethod.POST)
    ResponseEntity<Void> v1TaskPost(@ApiParam(value = "", required = true) @RequestBody Task task);


    @ApiOperation(value = "Get Individual Task", notes = "", response = Task.class, tags={ "task_controller", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Success", response = Task.class) })
    @RequestMapping(value = "/v1/task/{taskId}",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    ResponseEntity<Task> v1TaskGetById(@ApiParam(value = "", required = true) @PathVariable("taskId") Integer taskId);


    @ApiOperation(value = "Update a task", notes = "", response = Void.class, tags={ "task_controller", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Success", response = Void.class),
        @ApiResponse(code = 200, message = "Unexpected error", response = Void.class) })
    @RequestMapping(value = "/v1/task/{taskId}",
        produces = { "application/json" }, 
        method = RequestMethod.PUT)
    ResponseEntity<Void> v1TaskPut(@ApiParam(value = "", required = true) @PathVariable("taskId") Integer taskId,
                                   @ApiParam(value = "", required = true) @RequestBody Task task);


    @ApiOperation(value = "Close a task", notes = "Tell the databse to change the task state to CLOSE", response = Void.class, tags={ "task_controller", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Closed the task", response = Void.class),
        @ApiResponse(code = 200, message = "Unexpected Error", response = Void.class) })
    @RequestMapping(value = "/v1/task/{taskId}/$close",
        produces = { "application/json" }, 
        method = RequestMethod.POST)
    ResponseEntity<Void> v1TaskClose(@ApiParam(value = "", required = true) @PathVariable("taskId") Integer taskId);


    @ApiOperation(value = "Reorder a task", notes = "Change the rank of the task in the database", response = Void.class, tags={ "task_controller", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Reordered the task", response = Void.class),
        @ApiResponse(code = 200, message = "Unexpected Error", response = Void.class) })
    @RequestMapping(value = "/v1/task/{taskId}/$reorder",
        produces = { "application/json" }, 
        method = RequestMethod.POST)
    ResponseEntity<Void> v1TaskRerank(@ApiParam(value = "", required = true) @PathVariable("taskId") Integer taskId,
                                      @ApiParam(value = "", required = true) @RequestBody Task task);

    @ApiOperation(value = "Move a task up", notes = "Change the rank of the task to be higher then the next task in that project", response = Void.class, tags={ "task_controller", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Reordered the task", response = Void.class),
            @ApiResponse(code = 200, message = "Unexpected Error", response = Void.class) })
    @RequestMapping(value = "/v1/task/{taskId}/$moveUp",
            produces = { "application/json" },
            method = RequestMethod.POST)
    ResponseEntity<Void> v1TaskMoveUp(@ApiParam(value = "", required = true) @PathVariable("taskId") Integer taskId);

    @ApiOperation(value = "Move a task down", notes = "Change the rank of the task to be lower then the next task in that project", response = Void.class, tags={ "task_controller", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Reordered the task", response = Void.class),
            @ApiResponse(code = 200, message = "Unexpected Error", response = Void.class) })
    @RequestMapping(value = "/v1/task/{taskId}/$moveDown",
            produces = { "application/json" },
            method = RequestMethod.POST)
    ResponseEntity<Void> v1TaskMoveDown(@ApiParam(value = "", required = true) @PathVariable("taskId") Integer taskId);


    @ApiOperation(value = "Get all projects", notes = "Returns a list of all projects", response = Project.class, responseContainer = "List", tags={ "project_controller", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "An array of projects", response = Project.class),
            @ApiResponse(code = 500, message = "Unexpected Error", response = Void.class) })
    @RequestMapping(value = "/v1/project",
            produces = { "application/json" },
            method = RequestMethod.GET)
    ResponseEntity<List<Project>> v1ProjectGet(@ApiParam(value = "filter projectName with this string") String filter,
                                               @ApiParam(value = "filter the response by project state") List<Project.StateEnum> states);

    @ApiOperation(value = "Add a project", notes = "Add a project", response = Void.class, tags={ "project_controller", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = Void.class),
            @ApiResponse(code = 200, message = "Unexpected error", response = Void.class) })
    @RequestMapping(value = "/v1/project",
            produces = { "application/json" },
            method = RequestMethod.POST)
    ResponseEntity<Void> v1ProjectPost(@ApiParam(value = "", required = true) Project project);

    @ApiOperation(value = "Update a project", notes = "", response = Void.class, tags={ "project_controller", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = Void.class),
            @ApiResponse(code = 200, message = "Unexpected error", response = Void.class) })
    @RequestMapping(value = "/v1/project/{projectId}",
            produces = { "application/json" },
            method = RequestMethod.PUT)
    ResponseEntity<Void> v1ProjectPut(@ApiParam(value = "", required = true) Integer projectId,
                                   @ApiParam(value = "", required = true) Project project);


    @ApiOperation(value = "Create a backup of the database", notes = "Creates a full backup of the databse", response = Void.class, tags={ "maint_controller", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Reordered the task", response = Void.class),
            @ApiResponse(code = 200, message = "Unexpected Error", response = Void.class) })
    @RequestMapping(value = "/v1/db/$backup",
            produces = { "application/json" },
            method = RequestMethod.POST)
    ResponseEntity<Void> v1DatabaseBackup(@ApiParam(value = "", required = true) @RequestBody BackupLocation location);


}
