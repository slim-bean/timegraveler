#!/bin/bash -e

cd src/main/angular
#ng build --prod --base-href /tg/
ng build --prod
cd ../../../
mvn clean install
scp target/timegraveler-0.0.1-SNAPSHOT.jar ed@172.20.44.20:apps/tg/
