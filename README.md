# Timegraveler

I created this app to allow me to track my time at a very granular level with the goal of helping understand how I spend my time everyday and give me the information I needed to decide how I might want to change what I spend my time on.

Not finding anything available that did what I wanted, or did what I wanted and let me keep my data private, I decided to build something myself.

Originally this with built entirely in python, I really wanted to see what it was like developing REST webservices in a non-java environment.  Pretty quickly I realized that a python UI would be pretty terrible, reminding me of my days writing PHP, so I replaced the UI with an Angular2 single page app.

Not too much time later I got fed up with python for the backend, I would agree that getting out of the gate was a little easier with python than Java, but after that everything to me just seemed harder, or lacking in the flexibility/capability I had become familiar with in Java environments.

This app is the re-write of the python backend in Spring Boot, still keepin the Angular frontend (though now at Angular 5)

Sadly this app contains no automated tests :( :( I never intended to release it in any public fashion without them, however, I'm hoping to at least get some partial credit on a job interview for the app itself
 
## Running locally

You need a standalone hsqldb database for this app, you'll need to [download it](https://sourceforge.net/projects/hsqldb/files/hsqldb/hsqldb_2_4/) and start it with a command like this one (fixing the paths to match your local system):

```
java -cp ~/db/hsqldb-2.4.0/hsqldb/lib/hsqldb.jar org.hsqldb.Server --database.0 file:~/db/tg/tg --dbname.0 tg
```

My filesystem setup looks like this:

/home/user/db  
           ├── hsqldb-2.4.0  
           ├── hsqldb-2.4.0.zip  
           └── tg

You'll want to _mkdir tg_ I believe to avoid errors


To load the schema connect to the database with a SQL editor (jdbc:hsqldb:hsql://localhost:9001/tg) as the SA user (default password is an empty string) and run the `initial.sql` file in the sql directory and any migrations (in the proper order, oldest to newest) in the migrations sub directory, NOTE the passwords is changed in initial.sql and you'll need to use the new password to run the migrations.

You'll need to setup the NPM dependencies by moving to the angular directory and running `npm install`

To build the app for running directly from the jar, cd to the angular dir and run `ng build --prod`

**You have to make one change to run things directly (my setup has a reverse proxy and I just realized that breaks the local running of the app from the jar and I don't have time to fix this right now with another runtime config)**

Edit `application.properties` in src/main/resources and comment out `server.contextPath=/tg`

Then back to the root of the java project (with the pom.xml) and run `mvn clean install`

Now you can run the app with `java -jar target/timegraveler-0.0.1-SNAPSHOT.jar`

The app will be accessible at `http://localhost:5000/tg`

Or if you are running this out of an editor, you can fire up the spring boot app, and then fire up the webapp separately by running this command in the angular directory:

**Note, undo the change you made above for this to work, that is, uncomment the server.contextPath setting** 

`npm start`

This will let you connect to the webapp and give you live reload for development.




## Useful commands I care about but you probably won't

Connecting to prod db:

``ssh ed@172.20.44.20 -L 19001:localhost:9001``

Dumping prod DB to local:

```
cd ~/db
ssh ed@172.20.44.20 rm /home/ed/apps/tg/prod_export/*
curl -X POST --header 'Content-Type: application/json' -d '{ "compress": false, "directory": "/home/ed/apps/tg/prod_export/" }' 'https://prod.url/v1/db/$backup'
rm -r tg
mkdir tg
scp ed@172.20.44.20:apps/tg/prod_export/* tg/
```


Adding this to the npm start command lets me test from my phone using my existing internal `staging.oqqer.com` dns entry

```
--public staging.oqqer.com
```

**NOTE** You also have to add an entry in /etc/hosts for 127.0.0.1 for `staging.oqqer.com` so that the local host can resolve this too for liveupdates.

After a restart, in sys-net there is a script in /rw/config that will forward port 80 for both wired/wireless

There should already be permanent firewall rules in sys-firewall to send the traffic to the development VM

On the development VM forward port 80 to 4200 and let 4200 through the firewall:

```bash
sudo iptables -t nat -I PREROUTING -i eth0 -p tcp --dport 80 -j REDIRECT --to-ports 4200
sudo iptables -I INPUT -p tcp --dport 4200 -j ACCEPT
```